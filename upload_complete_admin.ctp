<?php
#getting max acne amount name (multiple works too)
$tempArray = [
    'forehead' => array_sum(str_split($order['Patient']['acne_forehead'])),
    'temple' => array_sum(str_split($order['Patient']['acne_temple'])),
    'nose' => array_sum(str_split($order['Patient']['acne_nose'])),
    'cheeks' => array_sum(str_split($order['Patient']['acne_cheeks'])),
    'chin' => array_sum(str_split($order['Patient']['acne_chin'])),
    'neck' => array_sum(str_split($order['Patient']['acne_neck'])),
    'chest' => array_sum(str_split($order['Patient']['acne_chest'])),
    'back' => array_sum(str_split($order['Patient']['acne_back']))];
$tempString='';
$tempString2='';
$maxValue = array_values(array_keys($tempArray, max($tempArray)));
foreach ($maxValue as $key => $value)
{
    $tempString2 .= $value.', ';
}
$tempString = substr_replace($tempString2 ,"", -2);
$gender="M";
switch ($order['Patient']['gender']){
case "M":
    $gender='a male';
        break;
    case "F":
    $gender='a female';
        break;
}

$tz  = new DateTimeZone('Europe/Kiev');
$age = DateTime::createFromFormat('Y-m-d', $order['Patient']['date_of_birth'], $tz)
    ->diff(new DateTime('now', $tz))
    ->y;

switch ($age){
    case $age == 1:
        $age = $age.=' year';
        break;
    case $age >= 2:
        $age = $age.=' years';
        break;
}

$UCSF=$order['Patient']['UCSF'];
$Questions=$order['Patient']['questions'];
switch($Questions)
{
    case ($order['Patient']['questions'] == ' '):
    case ($order['Patient']['questions'] == 0):
    case ($order['Patient']['questions'] == null):
        $Questions='';
        break;
    case ($order['Patient']['questions'] != null):
        $Questions = $order['User']['first_name'].' has the following questions about the acne care: '.$Questions.'<br>';
        break;
}


switch($UCSF) {
    case ($UCSF == null):
        $UCSF=$order['User']['first_name']." has never been at UCSF earlier.";
        break;
    case($UCSF == 'months'):
        $UCSF=$order['User']['first_name']." has never been at UCSF earlier.";
        break;
    default:
        $UCSF=$order['User']['first_name']." recalls being last seen at UCSF ".$order['Patient']['UCSF']." ago.";
}
$acneState = $order['Patient']['overall_state'];
switch($acneState){
    case ($order['Patient']['overall_state'] == "worse"):
        $acneState = "has worsened";
        break;
    case ($order['Patient']['overall_state'] == "better"):
        $acneState = " feels better";
        break;
    case ($order['Patient']['overall_state'] == "the same"):
        $acneState = " feels the same";
        break;
}


$test321=json_decode($order['Patient']['medications_not_interested']);
foreach ($test321 as $k=>$v){
    $new1[$v] = $v;
}
$medsNotString=join(' or ', array_filter(array_merge(array(join(', ', array_slice($new1, 0, -1))), array_slice($new1, -1)), 'strlen'));

$test123=json_decode($order['Patient']['medications']);
foreach ($test123 as $k => $v) {
    $new[$v] = $k;
}
$medsString=join(' and ', array_filter(array_merge(array(join(', ', array_slice($new, 0, -1))), array_slice($new, -1)), 'strlen'));

$test111=json_decode($order['Patient']['health_situations']);
foreach ($test111 as $k => $v) {
    $new3[$v] = $v;
}
$health_situations=join(' and ', array_filter(array_merge(array(join(', ', array_slice($new3, 0, -1))), array_slice($new3, -1)), 'strlen'));

?>





<p><?php echo date("F j, Y"); ?></p>
<br />
<p>Dear <?php echo $order['Provider']['first_name']; ?> <?php echo $order['Provider']['last_name']; ?>:</p>
<br />  
    Patient <?php echo $order['User']['first_name']."  ".$order['User']['last_name'].' identifying as '.$gender.', is '.$age.' old'.' with
     a '.$order['Patient']['first_acne'].'-year history of acne vulgaris.';?>
    <?php echo '<br>'.$UCSF.'<br>'; ?>
    In the last 3 months, <?php echo $order['User']['first_name'];?> has been consistently using <?php echo strtolower($order['Patient']['medications']);?><br/>
    His acne <?php echo $acneState; ?>, with the most involvement on the <?php echo $tempString; ?>. <br/>
    He is not interested in continuing <?php echo $medsNotString;?>.<br/>
    <?php echo $order['User']['first_name'].' notes the following current issues: '.$health_situations.'.';?><br/>
    Follow-up in  <?php echo $order['Patient']['to_be_seen'];?> weeks.<br/>
    <?php echo $Questions;?>

<br><p>P.S. You also have some photos to review now.</p>
<p>Order Id: <?php echo $order['Order']['id']; ?></p><br/>
Sincerely,</p>
<p>Your Clear Skin Concierge Team</p>
           