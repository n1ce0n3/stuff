# **Some basic QoL scripts.**  
_./addalias.sh_ - adds aliases from .alias to bashrc and updates your source.  
_./apache2wwwreset_ - resets the permissions of /var/www/ to default values: 644 to /var/www recursively, 755 to /var/www/, changes owner of /var/www/ recursively to www-data.  
_./advtail.sh_ - 'advanced' version of tail with a prompt and screen clear before start. Currently you have to move it to /use/bin/advtail, probably should add it back to aliases.  
