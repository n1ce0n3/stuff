#!/bin/bash
clear
echo -ne "\e[91m[*]\033[0m This script will append those aliases to your .bashrc file and automatically update your source after that.\n"
echo -e "$(cat .alias)\n"
read -p "Continue? [Y\n] " choice


if [[ "$choice" =~ ^Y$|^y$ ]] ; then
echo
$(echo 'cat .alias') >> ~/.bashrc
$(echo "source /home/$USER/.bashrc")
echo -ne "\e[91m[*]\033[0mSuccess! Please open a new terminal window for the aliases to work.\n"
else
echo -ne "\e[91m[*]\033[0mExiting. die();\n"
exit 0
fi
