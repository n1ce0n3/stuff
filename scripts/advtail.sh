#!/bin/bash

if [ "$#" -ne 1 ] ; then
	echo -e "Usage:\n"$(basename $0) "filename.txt"
	exit 1
fi
target="$1"
echo -ne "\e[91m[*]\033[0m"
read -p " Doing that will clear all the contents of '$target'. Continue ? [y/N] " choice


if [[ "$choice" =~ ^Y$|^y$ ]] ; then
clear
echo 0>$target
echo -e "\e[91m[*]\033[0mReading from $('pwd')/$target:\e[91m[*]\033[0m"
tail -f $target
else
echo "die();"
fi
