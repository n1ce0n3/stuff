#!/bin/bash
clear && apt-get update -y && apt-get install mysql-server -y --reinstall && apt-get install w3m -y --reinstall && service mysql start &&
clear && cd /etc/apache2/sites-enabled && cat 000-default.conf | sed -e 's/\/var\/www\/html/\/var\/www\/csc\/webroot/g' | sed '/example.com/,+1d' | sed -e 's/^\sServer.*$/\n        ServerName 127.0.0.1\n        ServerAdmin webmaster@localhost/g' > temp.txt && cat temp.txt > 000-default.conf  && rm temp.txt && cd - && clear && service apache2 restart && 
mysql -uroot << EOF
CREATE USER 'csc'@'%' IDENTIFIED BY 'c2cfDFgs3';
GRANT ALL PRIVILEGES ON *.* TO 'csc';
GRANT GRANT OPTION ON *.* TO 'csc';
FLUSH PRIVILEGES;
CREATE DATABASE csc;
USE csc;
exit
EOF
cd /var/www/vendor/pear-pear.cakephp.org/CakePHP &&
clear &&
$(echo 'apt-get install phpmyadmin -y');
mysql -ucsc -pc2cfDFgs3 csc < /var/www/csc.sql &&
clear &&
echo -ne "\e[91m[*]\033[0mApache2 settings:\e[91m[*]\033[0m\n" &&
cat /etc/apache2/sites-enabled/000-default.conf  | sed -n '10,12p' | sed -re 's/^\s+//g' &&
echo -e "\n\e[91m[*]\033[0mDatabases:\e[91m[*]\033[0m" &&
mysql -uroot << EOF
SHOW DATABASES;
exit
EOF
echo &&
bin/cake server -d /var/www/csc/webroot/ -app Cake -p 1337
