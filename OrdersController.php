<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeEvent', 'Event');
App::uses('OrderEventListener', 'Event');

/**
 * Orders Controller
 *
 * @property Order $Order
 */
class OrdersController extends AppController
{

    public $helpers = array('Excel');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('profile', 'profile', 'age_verify', 'identify', 'identify', 'save_meds_used', 'recommendations', 'recommendations', 'personalize', 'personalize', 'pregnant', 'preliminary_fine_aging', 'preliminary_fine_aging', 'consent', 'verify', 'goodrx_widget');
    }
    /*
    * index method
    *
    * @return void
    */
    //    public function index_old() {
    //        $this->Order->recursive = 0;
    //
    //        $this->set('orders', $this->Order->find('all', array(
    //            'order' => array('Order.date' => 'desc'),
    //            'conditions' => array(
    //                'Order.patient_id' => $this->current_user['Patient']['id'],
    //                'Order.deleted' => false,
    //
    //            )
    //        )));
    //
    //    }

    /*
* index method
*
* @return void
*/
    public function index()
    {
        $this->User->id = $this->Auth->user('id');
        $this->layout = "modern_layout";
        $this->Order->recursive = 0;

        $this->set('orders', $this->Order->find('all', array(
            'order' => array('Order.date' => 'desc'),
            'conditions' => array(
                'Order.user_id' => $this->current_user['User']['id'],
                'Order.deleted' => false,

            )
        )));

        $this->set('user', $this->User->find('first', array(
            'conditions' => array(
                'User.id' => $this->current_user['User']['id']
            )
        )));
    }

    /**
     * Sets the order and meds for a patient to view one of their orders.
     * @param string $id
     * @throws NotFoundException
     */
//    public function view_old($id = null) {
//        if ($id != null) {
//
//            $order = $this->Order->findByIdAndPatientId($id,$this->current_user['Patient']['id']);
//            if(isset($order)) {
//
//                $treatments = $this->Order->Treatment->find('first', array(
//                    'conditions' => array(
//                        'Treatment.id' => $order['Treatment']['id']
//                    )));
//
//
//                $user = $this->Order->User->find('first', array(
//                    'conditions' => array(
//                        'User.id' => $this->current_user['Patient']['user_id']
//                    )
//                ));
//                $patient_photo = $this->Order->Patient->PatientPhoto->find('first', array(
//                    'conditions' => array(
//                        'PatientPhoto.patient_id' => $this->current_user['Patient']['id']
//                    ),
//                    'order' => 'PatientPhoto.created DESC',
//                    'limit' => 1
//                ));
//                $order_meds = $this->Order->OrdersMed->find('all', array(
//                    'conditions' => array(
//                        'OrdersMed.id' => $order['Order']['id']
//                    )
//                ));
//
//                $meds = array();
//                for ($i = 0; $i< count($order_meds); $i++) {
//                    array_push($meds, $order_meds[$i]['Med']['name']);
//                }
//
//                $order['User'] = $user['User'];
//                $order['PatientPhoto'] = $patient_photo['PatientPhoto'];
//                $order['Med'] = $meds;
//                $order['Treatment'] = $treatments;
//
//
//                $this->set(compact('order','meds'));
//            } else {
//                $this->Session->setFlash(__('We could not find that order.'), 'alert', array(
//                    'plugin' => 'BoostCake',
//                    'class' => 'alert-danger'
//                ));
//            }
//        } else {
//            throw new NotFoundException();
//        }
//    }

    /**
     * Sets the order and meds for a patient to view one of their orders.
     * @param string $id
     * @throws NotFoundException
     */
    public function view($id = null)
    {
        $this->layout = "modern_layout";
        if ($id != null) {

            $order = $this->Order->findByIdAndPatientId($id, $this->current_user['Patient']['id']);
            if (isset($order)) {

                $treatments = $this->Order->Treatment->find('first', array(
                    'conditions' => array(
                        'Treatment.id' => $order['Treatment']['id']
                    )));


                $user = $this->Order->User->find('first', array(
                    'conditions' => array(
                        'User.id' => $this->current_user['Patient']['user_id']
                    )
                ));
                $patient_photo = $this->Order->Patient->PatientPhoto->find('first', array(
                    'conditions' => array(
                        'PatientPhoto.patient_id' => $this->current_user['Patient']['id']
                    ),
                    'order' => 'PatientPhoto.created DESC',
                    'limit' => 1
                ));
                $order_meds = $this->Order->OrdersMed->find('all', array(
                    'conditions' => array(
                        'OrdersMed.id' => $order['Order']['id']
                    )
                ));

                $meds = array();
                for ($i = 0; $i < count($order_meds); $i++) {
                    array_push($meds, $order_meds[$i]['Med']['name']);
                }

                $order['User'] = $user['User'];
                $order['PatientPhoto'] = $patient_photo['PatientPhoto'];
                $order['Med'] = $meds;
                $order['Treatment'] = $treatments;


                $this->set(compact('order', 'meds'));
            } else {
                $this->Session->setFlash(__('We could not find that order.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger custom'
                ));
            }
        } else {
            throw new NotFoundException();
        }
    }

    public function treatment($id = null, $problem = null)
    {

        if ($id != null) {

            $order = $this->Order->findByIdAndPatientId($id, $this->current_user['Patient']['id']);

            if (isset($order) && $order['Order']['type'] == 'ef') {
                $this->autoRender = false;

                $file = ROOT . '/csc/treatments/Treatment_ef.pdf';
                if (file_exists($file)) {
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename="Eyelashes_treatment.pdf"');
                    readfile($file);

                    return;
                }
            }

            if (isset($order) && $order['Order']['type'] == 'fal_ef') {
                $this->autoRender = false;

                switch ($problem) {
                    case 'fal':
                        $file = ROOT . '/csc/treatments/Fine Aging Lines - Tretinoin for younger skin.pdf';
                        $filename = 'Fine_Aging_Lines_treatment.pdf';
                        break;
                    case 'ef':
                        $file = ROOT . '/csc/treatments/Treatment_ef.pdf';
                        $filename = 'Eyelashes_treatment.pdf';
                        break;
                    default:
                        $file = null;
                        break;
                }
                if (file_exists($file)) {
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename="' . $filename . '"');
                    readfile($file);

                    return;
                }
            }

            if (isset($order) && $order['Order']['type'] == 'msd_ef') {
                $this->autoRender = false;
                switch ($problem) {
                    case 'msd':
                        $file = ROOT . '/csc/treatments/Treatment_msd.pdf';
                        $filename = 'Triluma_treatment.pdf';
                        break;
                    case 'ef':
                        $file = ROOT . '/csc/treatments/Treatment_ef.pdf';
                        $filename = 'Eyelashes_treatment.pdf';
                        break;
                    default:
                        $file = null;
                        break;
                }
                if (file_exists($file)) {
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename="' . $filename . '"');
                    readfile($file);

                    return;
                }
            }

            if (isset($order) && $order['Order']['type'] == 'msd') {
                $this->autoRender = false;

                $file = ROOT . '/csc/treatments/Treatment_msd.pdf';
                if (file_exists($file)) {
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename="Triluma_treatment.pdf"');
                    readfile($file);

                    return;
                }
            }

            if (isset($order) && $order['Order']['type'] == 'fal') {
                $this->autoRender = false;

                $file = ROOT . '/csc/treatments/Fine Aging Lines - Tretinoin for younger skin.pdf';
                if (file_exists($file)) {
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename="Fine_Aging_Lines_treatment.pdf"');
                    readfile($file);

                    return;
                }
            }

            if (isset($order) && isset($order['Treatment']) && $order['Treatment']['id'] !== null) {

                $this->autoRender = false;

                $file = ROOT . '/csc/treatments/' . $order['Treatment']['pdf'];
                if (file_exists($file)) {
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename="Acne_treatment.pdf"');
                    readfile($file);

                    return;
                }
            }
        }
        throw new NotFoundException();
    }

    public function reject_treatment($id = null)
    {
        if ($id != null) {
            $order = $this->Order->findByIdAndPatientId($id, $this->current_user['Patient']['id']);

            if (isset($order) && $order['Order']['completed_date'] !== null) {
                $appointmentDate = new DateTime($order['Order']['completed_date'] . ' + 48 hours');
                $date = new DateTime('now');
                $interval = $date->diff($appointmentDate);

                if ($interval->invert == true) {
                    $this->Session->setFlash(__('Something went wrong'), 'alert', array(
                        'plugin' => 'BoostCake',
                        'class' => 'alert-danger'
                    ));
                    return $this->redirect(array('controller' => 'orders', 'action' => 'view', $id));
                }

                $orderForSave = array(
                    'id' => (int)$order['Order']['id'],
                    'status' => 'Rejected',
                    'completed_date' => date("Y-m-d H:i:s"));

                $this->Order->save($orderForSave);

                $from = array(Configure::read('App.email') => 'Clear Skin Concierge');

                App::uses('CakeEmail', 'Network/Email');
                $Email = new CakeEmail('smtp');
                $Email->to($order['Provider']['email'])
                    ->from($from)
                    ->bcc('motik@6mail.top')
                    ->subject('Patient rejected treatment.' . ' ' . Configure::read('App.domain'))
                    ->template('patient_rejected_treatment', null)
                    ->viewVars(array('order' => $order))
                    ->send();
            }

            $this->Session->setFlash(__('Treatment is rejected.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-success'
            ));
        } else {
            $this->Session->setFlash(__('Bad ID of order.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-success'
            ));
        }


        return $this->redirect(array('controller' => 'orders', 'action' => 'view', $id));
    }

    public function accept_treatment($id = null)
    {
        if ($id != null) {
            $order = $this->Order->findByIdAndPatientId($id, $this->current_user['Patient']['id']);

            if (isset($order) && $order['status'] !== 'Completed') {

                $orderForSave = array(
                    'id' => (int)$order['Order']['id'],
                    'status' => 'Accepted',
                    'completed_date' => date("Y-m-d H:i:s"));

                $this->Order->save($orderForSave);

                $from = array(Configure::read('App.email') => 'Clear Skin Concierge');

                App::uses('CakeEmail', 'Network/Email');
                $Email = new CakeEmail('smtp');
                $Email->to($order['Provider']['email'])
                    ->from($from)
                    ->bcc('motik@6mail.top')
                    ->subject('Patient accepted treatment.' . ' ' . Configure::read('App.domain'))
                    ->template('patient_accepted_treatment', null)
                    ->viewVars(array('order' => $order))
                    ->send();
            }

            $this->Session->setFlash(__('Treatment is accepted.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-success'
            ));
        } else {
            $this->Session->setFlash(__('Bad ID of order.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-success'
            ));
        }

        return $this->redirect(array('controller' => 'orders', 'action' => 'view', $id));
    }

    public function delete($id = null)
    {
        $order = $this->Order;
        // Validate owner
        if ($order->hasAny(array(
            'id' => $id,
            'patient_id' => $this->current_user['Patient']['id'],
        ))) {

            $order->set('id', $id);
            if ($order->complete($order::STATUS_CANCELLED)) {

                //sent email to provider about cancel of paid order
                $order_for_email = $this->Order->find('first', array('conditions' => array('Order.id' => $id)));
                //if(strpos($this->request->url, 'paid') !== false){
                $this->Order->getEventManager()->dispatch(
                    new CakeEvent(
                        'Controller.Order.cancelPaidOrder',
                        $this,
                        array_merge(
                            array('order' => $order_for_email),
                            array('user' => $this->current_user),
                            array('passParams' => true)
                        )));
                //}
                $this->Session->setFlash(__('Order is cancelled.'), 'default', array('class' => 'alert alert-success'));
            } else {
                $this->Session->setFlash(__('Order is not cancelled.'), 'default', array('class' => 'alert alert-warning'));
            }
        }

        if (strpos($this->request->url, 'dashboard') != false) {
            return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
        } else {
            return $this->redirect(array('controller' => 'orders', 'action' => 'index'));
        }
    }

    public function delete_modern($id = null)
    {
        $order = $this->Order;
        // Validate owner
        if ($order->hasAny(array(
            'id' => $id,
            'patient_id' => $this->current_user['Patient']['id'],
        ))) {

            $order->set('id', $id);
            if ($order->complete($order::STATUS_CANCELLED)) {

                //sent email to provider about cancel of paid order
                $order_for_email = $this->Order->find('first', array('conditions' => array('Order.id' => $id)));
                //if(strpos($this->request->url, 'paid') !== false){
                $this->Order->getEventManager()->dispatch(
                    new CakeEvent(
                        'Controller.Order.cancelPaidOrder',
                        $this,
                        array_merge(
                            array('order' => $order_for_email),
                            array('user' => $this->current_user),
                            array('passParams' => true)
                        )));
                //}
                $this->Session->setFlash(__('Order is cancelled.'), 'default', array('class' => 'alert alert-success custom'));
            } else {
                $this->Session->setFlash(__('Order is not cancelled.'), 'default', array('class' => 'alert alert-warning custom'));
            }
        }

        if (strpos($this->request->url, 'dashboard') != false) {
            return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
        } else {
            return $this->redirect(array('controller' => 'orders', 'action' => 'index'));
        }
    }

    public function admin_cancel($id = null)
    {
        $this->autoRender = false;
        $order = $this->Order;

        $order->set('id', $id);
        if ($order->complete($order::STATUS_CANCELLED)) {
            //sent email to provider about cancel of paid order
            $order_for_email = $this->Order->find('first', array('conditions' => array('Order.id' => $id)));
            $this->Order->getEventManager()->dispatch(
                new CakeEvent(
                    'Controller.Order.cancelPaidOrderByAdministration',
                    $this,
                    array_merge(
                        array('order' => $order_for_email),
                        array('passParams' => true)
                    )));
            $this->Session->setFlash(__('Order is cancelled.'), 'default', array('class' => 'alert alert-success custom'));
        } else {
            $this->Session->setFlash(__('Order is not cancelled.'), 'default', array('class' => 'alert alert-warning custom'));
        }

        return $this->redirect(array('action' => 'index', 'admin' => true));
    }

    /**
     * Gather the initial profile information that is needed to identify the skin condition.
     * @return Ambigous <mixed, void>
     */
//    public function profile_old() {
//		// TODO: probably param "wizard_step" is not used in templates
//        $this->set('wizard_step', 'Profile');
//        //Check to see if the Patient is logged in.
//
//        /*
//         * If the Patient is logged in this is a reorder.
//         * Check to see if the Patient is a female.
//         * If so, set a flag to show the pregnacy question.
//         */
//
//        if ($user = $this->Auth->user()) {
//            $loggedIn = true;
//            $gender = $user['Patient']['gender'];
//            $this->set(compact('loggedIn','gender'));
//        }
//
//        $title_for_layout = 'Order Online Acne Treatment and Anti Aging Creams | Dermatologist CA';
//        $description_for_layout = 'Order your various online dermatologist items for Acne Treatment, Anti Aging Creams and Eyelash Growth using retinol, retin a and tretinoin cream in CA';
//        $this->set(compact('title_for_layout','description_for_layout'));
//
//        if ($this->request->is('post')) {
//
//            // New order clear old data from session
//            $this->Session->delete('User');
//            $this->Session->delete('Patient');
//            $this->Session->delete('Order');
//            $this->Session->delete('age');
//            $this->Session->delete('overview-done');
//            $this->Session->delete('paid');
//            $this->Session->delete('complete');
//            $this->Session->delete('payment_error');
//            $this->Session->delete('current_action');
//            $this->Session->delete('current_controller');
//
//            if (!empty($user)) {
//                $this->Session->write('User.id', $this->request->data['User']['id']);
//                $this->Session->write('Patient.id', $this->request->data['Patient']['id']);
//            }
//
//            $this->Order->validates();
//            $this->Order->Patient->User->validates();
//            $this->Order->Patient->validates();
//            $this->Session->write('User.over_18', $this->request->data['User']['over_18']);
//            $patient['gender'] = $this->request->data['Patient']['gender'];
//            $patient['state'] = $this->request->data['Patient']['state'];
//            $order['Order']['user_id'] = $this->Auth->user('id');
//            $order['Order']['patient_id'] = $this->Order->getPatientId($this->Auth->user('id'));
//            $order['Order']['provider_id'] = $this->Order->getProviderId($this->Auth->user('id'));
//            $order['Order']['consent'] = 0;
//
//            $order_type = $this->request->data['Order']['type'];
//            //$order['Order']['fine_age'] = $this->request->data['Order']['fine_age'];
//            $order['Order']['type'] = $order_type;
//            $order['Order']['not_pregnant'] = $this->request->data['Order']['not_pregnant'];
//            ($order_type == 'fal')?($order['Order']['fine_age'] = 1):($order['Order']['fine_age'] = 0);
//            if ($order_type != 'acne') {
//                $acne['Order'] = array(
//                    'acne_forehead' => '00000',
//                    'acne_cheeks' => '00000',
//                    'acne_chin' => '00000',
//                    'acne_chest' => '00000',
//                    'acne_back' => '00000',
//                    'acne_neck' => '00000',
//                    'acne_nose' => '00000',
//                    'acne_temple' => '00000'
//                );
//                switch($order_type){
//                    case 'fal':
//                        $order_status = 'fine_aging_lines';
//                        break;
//                    case 'msd':
//                        $order_status = 'mild_skin_darkening';
//                        break;
//                    case 'ef':
//                        $order_status = 'eyelash_fullness';
//                        break;
//                    default:
//                        $order_status = 'acne';
//                        break;
//                }
//                $order['Order']['status'] = $order_status;
//                $this->Session->write('Order',am($order['Order'],$acne['Order']));
//                $this->Session->write('Patient',am($patient,$acne['Order']));
//                return $this->redirect(array('action' => 'preliminary_fine_aging'));
//
//            } else {
//                $order['Order']['status'] = 'identify';
//                $this->Session->write('Patient',$patient);
//                $this->Session->write('Order',$order['Order']);
//                return $this->redirect(array('action' => 'identify'));
//            }
//        }
//
//    }

    public function profile()
    {
        $this->layout = 'modern_layout';
        // TODO: probably param "wizard_step" is not used in templates
        $this->set('wizard_step', 'Profile');
        //Check to see if the Patient is logged in.

        /*
         * If the Patient is logged in this is a reorder.
         * Check to see if the Patient is a female.
         * If so, set a flag to show the pregnacy question.
         */


        if ($user = $this->Auth->user()) {
            $loggedIn = true;
            $gender = $user['Patient']['gender'];
            $user = $this->current_user;
            $this->set(compact('loggedIn', 'gender', 'user'));
        }

        $title_for_layout = 'Order Online Acne Treatment and Anti Aging Creams | Dermatologist CA';
        $description_for_layout = 'Order your various online dermatologist items for Acne Treatment, Anti Aging Creams and Eyelash Growth using retinol, retin a and tretinoin cream in CA';
        $this->set(compact('title_for_layout', 'description_for_layout'));

        if ($this->request->is('post')) {

            // New order clear old data from session
            $this->Session->delete('User');
            $this->Session->delete('Patient');
            $this->Session->delete('Order');
            $this->Session->delete('age');
            $this->Session->delete('overview-done');
            $this->Session->delete('paid');
            $this->Session->delete('complete');
            $this->Session->delete('payment_error');
            $this->Session->delete('current_action');
            $this->Session->delete('current_controller');

            if (!empty($user)) {
                $this->Session->write('User.id', $this->request->data['User']['id']);
                $this->Session->write('Patient.id', $this->request->data['Patient']['id']);
            }

            $this->Order->validates();
            $this->Order->Patient->User->validates();
            $this->Order->Patient->validates();
            $this->Session->write('User.over_18', $this->request->data['User']['over_18']);
            $patient['gender'] = $this->request->data['Patient']['gender'];
            $patient['state'] = $this->request->data['Patient']['state'];
            $order['Order']['user_id'] = $this->Auth->user('id');
            $order['Order']['patient_id'] = $this->current_user['Patient']['id'];//$this->Order->getPatientId($this->Auth->user('id'));
            $order['Order']['provider_id'] = $this->current_user['Patient']['provider_id'];//$this->Order->getProviderId($this->Auth->user('id'));
            $order['Order']['consent'] = 0;

            $order_type = $this->request->data['Order']['type'];

            //$order['Order']['fine_age'] = $this->request->data['Order']['fine_age'];
            $order['Order']['type'] = $order_type;
            $order['Order']['not_pregnant'] = (is_null($this->request->data['Order']['not_pregnant'])) ? 1 : $this->request->data['Order']['not_pregnant'];
            ($order_type == 'fal') ? ($order['Order']['fine_age'] = 1) : ($order['Order']['fine_age'] = 0);

            if ($order_type != 'acne') {
                $acne['Order'] = array(
                    'acne_forehead' => '00000',
                    'acne_cheeks' => '00000',
                    'acne_chin' => '00000',
                    'acne_chest' => '00000',
                    'acne_back' => '00000',
                    'acne_neck' => '00000',
                    'acne_nose' => '00000',
                    'acne_temple' => '00000'
                );
                switch ($order_type) {
                    case 'fal':
                        $order_status = 'fine_aging_lines';
                        break;
                    case 'msd':
                        $order_status = 'mild_skin_darkening';
                        break;
                    case 'ef':
                        $order_status = 'eyelash_fullness';
                        break;
                    default:
                        $order_status = 'acne';
                        break;
                }
                $order['Order']['status'] = $order_status;
                $this->Session->write('Order', am($order['Order'], $acne['Order']));
                $this->Session->write('Patient', am($patient, $acne['Order']));
                return $this->redirect(array('action' => 'preliminary_fine_aging'));

            } else {
                $order['Order']['status'] = 'identify';
                $this->Session->write('Patient', $patient);
                $this->Session->write('Order', $order['Order']);
                return $this->redirect(array('action' => 'identify'));
            }
        }

    }

    /*
    * age_verify method
    *
    * @todo If the we have a User, we can skip this step. However redirecting to payment isn't the
    * process. We need to redirect to the next step
    *
    * @deprecated This has been replaced by the profile method.
    *
    * @return void
    */
    public function age_verify()
    {
        /**
         * If we have an open order, redirect to the next step
         */
        //debug($this->Csc->getNextOrderStep());
        /*if(isset($this->current_user) && $this->Session->check('Order.id') && $this->Csc->getNextOrderStep()) {
            return $this->redirect(array('controller' => 'orders', 'action' => $this->Csc->getNextOrderStep()));
        }
        else if($this->Session->read('Order.id')) {
            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
        }*/
        $this->Session->write('overview-done', "DONE");
        $this->set('nav_step', 1);
        if ($this->request->is('post')) {
            if (!empty($this->request->data['Order']['accept_age'])) {
                $this->Session->write('age', 'over18');
                $this->Session->write('Order.status', 'identify');
                return $this->redirect(array('action' => 'identify'));
            }
        }
        /**
         * @todo We really don't need to set this in the controller. We can access the session from the view.
         */
        $fine_age = $this->Session->read('Order.fine_age');
        $this->set(compact('fine_age'));
    }


    /*
    * identify method
    *
    * @return void
    */
//    public function identify_old() {
//
//        $this->Session->write('current_controller', 'orders');
//        $this->Session->write('current_action', 'identify');
//
//        $this->set('wizard_step', 'Identify');
//        $acne = $this->_initializeAcne();
//        /**
//         * @todo Why are we setting overview done here?
//         */
//        $this->Session->write('overview-done', "DONE");
//
//        if($this->Session->read('Order.id')) {
//            return $this->redirect(array('controller' => 'users', 'action' => 'login'));
//        }
//        //$this->set('nav_step', 1);
//        if ($this->request->is('post')) {
//
//            if ($this->request->data['Order']['acne_forehead'] != '00000' ||
//                $this->request->data['Order']['acne_cheeks'] != '00000' ||
//                $this->request->data['Order']['acne_chin'] != '00000' ||
//                $this->request->data['Order']['acne_chest'] != '00000' ||
//                $this->request->data['Order']['acne_back'] != '00000' ||
//                $this->request->data['Order']['acne_neck'] != '00000' ||
//                $this->request->data['Order']['acne_nose'] != '00000' ||
//                $this->request->data['Order']['acne_temple'] != '00000'
//            ) {
//                $acne = array(
//                    'acne_forehead' => $this->request->data['Order']['acne_forehead'],
//                    'acne_cheeks' => $this->request->data['Order']['acne_cheeks'],
//                    'acne_chin' => $this->request->data['Order']['acne_chin'],
//                    'acne_chest' => $this->request->data['Order']['acne_chest'],
//                    'acne_back' => $this->request->data['Order']['acne_back'],
//                    'acne_neck' => $this->request->data['Order']['acne_neck'],
//                    'acne_nose' => $this->request->data['Order']['acne_nose'],
//                    'acne_temple' => $this->request->data['Order']['acne_temple'],
//
//                );
//                $order = am($this->Session->read('Order'), $acne);
//                $order['status'] = 'recommendations';
//                $this->Session->write('Order', $order);
//                $patient = am($acne,$this->Session->read('Patient'));
//                $this->Session->write('Patient',$patient);
//                $this->redirect(array('action' => 'recommendations'));
//            } else {
//                $this->Session->setFlash(__('You must identify at least one kind of acne.'), 'alert', array(
//                    'plugin' => 'BoostCake',
//                    'class' => 'alert-danger'
//                ));
//            }
//        }
//        else {
//            if ($this->Session->read('Order.acne_forehead')) {
//                $acne = $this->_getOrderAcne();
//
//            }
//
//
//        }
//
//        $fine_age = $this->Session->read('Order.fine_age');
//        $this->set(compact('acne','fine_age'));
//    }


    /*
* identify method
*
* @return void
*/
    public function identify()
    {
        $this->layout = 'modern_layout';
        $this->Session->write('current_controller', 'orders');
        $this->Session->write('current_action', 'identify');

        $this->set('wizard_step', 'Identify');
        $acne = $this->_initializeAcne();
        $this->Session->write('Choice', '');
        /**
         * @todo Why are we setting overview done here?
         */
        $this->Session->write('overview-done', "DONE");

        if ($this->Session->read('Order.id')) {
            return $this->redirect(array('controller' => 'pages', 'action' => 'home'));
        }
        //$this->set('nav_step', 1);
        if ($this->request->is('post')) {

            if ($this->request->data['Order']['acne_forehead'] != '00000' ||
                $this->request->data['Order']['acne_cheeks'] != '00000' ||
                $this->request->data['Order']['acne_chin'] != '00000' ||
                $this->request->data['Order']['acne_chest'] != '00000' ||
                $this->request->data['Order']['acne_back'] != '00000' ||
                $this->request->data['Order']['acne_neck'] != '00000' ||
                $this->request->data['Order']['acne_nose'] != '00000' ||
                $this->request->data['Order']['acne_temple'] != '00000'
            ) {
                $acne = array(
                    'acne_forehead' => $this->request->data['Order']['acne_forehead'],
                    'acne_cheeks' => $this->request->data['Order']['acne_cheeks'],
                    'acne_chin' => $this->request->data['Order']['acne_chin'],
                    'acne_chest' => $this->request->data['Order']['acne_chest'],
                    'acne_back' => $this->request->data['Order']['acne_back'],
                    'acne_neck' => $this->request->data['Order']['acne_neck'],
                    'acne_nose' => $this->request->data['Order']['acne_nose'],
                    'acne_temple' => $this->request->data['Order']['acne_temple'],

                );
                $order = am($this->Session->read('Order'), $acne);
                $order['status'] = 'recommendations';
                $this->Session->write('Order', $order);
                $patient = am($acne, $this->Session->read('Patient'));
                $this->Session->write('Patient', $patient);
                $this->redirect(array('action' => 'personalize'));
            } else {
                $this->Session->setFlash(__('You must identify at least one kind of acne.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger'
                ));
            }
        } else {
            if ($this->Session->read('Order.acne_forehead')) {
                $acne = $this->_getOrderAcne();

            }


        }

        $fine_age = $this->Session->read('Order.fine_age');
        $this->set(compact('acne', 'fine_age'));
    }


    /*
    * recommendations method
    *
    * @return void
    */
//    public function recommendations_old() {
//
//        $this->Session->write('current_controller', 'orders');
//        $this->Session->write('current_action', 'recommendations');
//
//        /**
//         * If we have an open order, redirect to the next step
//         */
//        if(isset($current_user) && $this->Session->check('Order.id') && $this->Csc->getNextOrderStep()) {
//            return $this->redirect(array('controller' => 'orders', 'action' => $this->Csc->getNextOrderStep()));
//        }
//
//        $acne = $this->_getOrderAcne();
//        $acne_types = array();
//        for($index = 0; $index < 5; $index++) {
//            if($acne['acne_forehead'][$index] == 1 ||
//                $acne['acne_cheeks'][$index] == 1 ||
//                $acne['acne_chin'][$index] == 1 ||
//                $acne['acne_chest'][$index] == 1 ||
//                $acne['acne_back'][$index] == 1 ||
//                $acne['acne_neck'][$index] == 1 ||
//                $acne['acne_nose'][$index] == 1 ||
//                $acne['acne_temple'][$index] == 1
//            ) {
//                $acne_types[$index] = "1";
//            }
//            else {
//                $acne_types[$index] = "0";
//            }
//        }
//        $this->Session->write('Order.status', 'personalize');
//
//        $this->set(compact('acne', 'acne_types'));
//    }

    /*
* recommendations method
*
* @return void
*/
    public function recommendations()
    {
        $this->layout = 'modern_layout';
        $this->Session->write('current_controller', 'orders');
        $this->Session->write('current_action', 'recommendations');

        /**
         * If we have an open order, redirect to the next step
         */
        if (isset($current_user) && $this->Session->check('Order.id') && $this->Csc->getNextOrderStep()) {
            return $this->redirect(array('controller' => 'orders', 'action' => $this->Csc->getNextOrderStep()));
        }

        $acne = $this->_getOrderAcne();
        $acne_types = array();
        for ($index = 0; $index < 5; $index++) {
            if ($acne['acne_forehead'][$index] == 1 ||
                $acne['acne_cheeks'][$index] == 1 ||
                $acne['acne_chin'][$index] == 1 ||
                $acne['acne_chest'][$index] == 1 ||
                $acne['acne_back'][$index] == 1 ||
                $acne['acne_neck'][$index] == 1 ||
                $acne['acne_nose'][$index] == 1 ||
                $acne['acne_temple'][$index] == 1
            ) {
                $acne_types[$index] = "1";
            } else {
                $acne_types[$index] = "0";
            }
        }
        $this->Session->write('Order.status', 'personalize');

        $this->set(compact('acne', 'acne_types'));
    }


    /*
    * @return void
    */
    public function pregnant()
    {
        /**
         * If we have an open order, redirect to the next step
         */
        if (isset($current_user) && $this->Session->check('Order.id') && $this->Csc->getNextOrderStep()) {
            return $this->redirect(array('controller' => 'orders', 'action' => $this->Csc->getNextOrderStep()));
        }

        if ($this->request->is('post')) {
            if ($this->request->data['Order']['not_pregnant'] == 'Yes') {
                $this->Session->write('Order.not_pregnant', false);
            } else if ($this->request->data['Order']['not_pregnant'] == 'No') {
                $this->Session->write('Order.not_pregnant', true);
            }
            $this->Session->write('Order.status', 'personalize');
            $this->redirect(array('action' => 'personalize'));
        }

        $fine_age = $this->Session->read('Order.fine_age');
        $this->set(compact('fine_age'));
    }

    /*
     * Determine Acne Class,
     * Determine recommended treatment, alternative medications
     * Assign the final treatment
     */
//    public function personalize_old() {
//
//        $this->Session->write('current_controller', 'orders');
//        $this->Session->write('current_action', 'personalize');
//
//        // the final choice of treatment via user preferences
//        if ($this->request->is('post')) {
//            $order = am($this->Session->read('Order'),$this->request->data['Order']);
//
//            $treatment = $this->Order->Treatment->find('first', array(
//                'conditions' => array(
//                    'Treatment.wash' => $order['wash'],
//                    'Treatment.antibiotic' => $order['antibiotic'],
//                    'Treatment.retinoid' => $order['retinoid'],
//                    'Treatment.acne_class_id' => $order['acne_class_id'],
//                ),
//                'order' => 'Treatment.priority'
//            ));
//
//            if(count($treatment) == 0) {
//                $this->Session->setFlash(__('If you only want to use benzoyl peroxide or salicyclic acid,
//                    no prescriptions from Clear Concierge are necessary! Although we would love to have you as a patient,
//                    you can pick these medications up as creams or washes from most local pharmacies, without signing up with us.
//                    Please select additional medications if you wish to continue.'), 'alert', array(
//                        'plugin' => 'BoostCake',
//                        'class' => 'alert-danger'
//                ));
//                return $this->redirect(array('action' => 'personalize'));
//            }
//
//            $order['treatment_id'] = $treatment['Treatment']['id'];
//            $order['status'] = 'consent';
//
//            $this->Session->write('Order', $order);
//
//            //write current user's choice
//            $this->Session->write('Choice.wash',$this->request->data['Order']['wash']);
//            $this->Session->write('Choice.antibiotic',$this->request->data['Order']['antibiotic']);
//            $this->Session->write('Choice.retinoid',$this->request->data['Order']['retinoid']);
//            $this->Session->write('Choice.not_allergic',$this->request->data['Order']['not_allergic']);
//            $this->Session->write('Choice.doxy_no_condition',$this->request->data['Order']['doxy_no_condition']);
//            $this->Session->write('Choice.doxy_no_medication',$this->request->data['Order']['doxy_no_medication']);
//
//            if(!$this->current_user) {
//                return $this->redirect(array('controller' => 'users', 'action' => 'add'));
//            } else {
//                $current_user = $this->current_user;
//
//                if($this->Session->read('Patient') && $this->Session->read('Order') && $this->current_user) {
//                    $provider = $this->User->getProvider();
//                    $patient = $current_user['Patient'];
//                    $order['user_id'] = (integer)$current_user['User']['id'];
//                    $order['patient_id'] = (integer)$patient['id'];
//                    $order['provider_id'] = (integer)$provider['User']['id'];
//                    $order = $this->Order->save($order);
//
//                    $this->Session->write('Order', $order['Order']);
//                }
//
//                return $this->redirect(array('action' => 'consent'));
//            }
//        }
//
//        // determine acne class
//        $this->loadModel('AcneClass');
//        $acne = $this->_getOrderAcne();
//
//        $acne_class = $this->AcneClass->determineAcneClass($acne, !$this->Session->read('Order.not_pregnant'));
//
//        $this->Session->write('Order.acne_class_id', $acne_class['id']);
//
//        $treatments = $this->Order->Treatment->find('all', array(
//            'conditions' => array('Treatment.acne_class_id' => $acne_class['id']),
//            'order' => 'Treatment.priority',
//            'contain' => array('Wash', 'Antibiotic', 'Retinoid')
//        ));
//
//        // default treatment has priority = 0, ordering by priority
//        $recommended = $treatments[0];
//
//        // get alternative meds
//        // construction "$wash[$treatment['Wash']['id']]" used for exclude duplicate meds
//        foreach($treatments as $treatment) {
//            if($recommended['Wash']['id'] != $treatment['Wash']['id'] && $treatment['Wash']['id'])
//                $wash[$treatment['Wash']['id']] = $treatment['Wash'];
//            if($recommended['Antibiotic']['id'] != $treatment['Antibiotic']['id'] && $treatment['Antibiotic']['id'])
//                $antibiotic[$treatment['Antibiotic']['id']] = $treatment['Antibiotic'];
//            if($recommended['Retinoid']['id'] != $treatment['Retinoid']['id'] && $treatment['Retinoid']['id'])
//                $retinoid[$treatment['Retinoid']['id']] = $treatment['Retinoid'];
//        }
//        $acne_class = $acne_class['id'];
//        $this->set(compact('acne_class', 'recommended', 'wash', 'antibiotic', 'retinoid', 'user_choice'));
//    }

    /*
 * Determine Acne Class,
 * Determine recommended treatment, alternative medications
 * Assign the final treatment
 */
    public function personalize()
    {
        $this->layout = "modern_layout";
        $this->Session->write('current_controller', 'orders');
        $this->Session->write('current_action', 'personalize');

        // the final choice of treatment via user preferences
        if ($this->request->is('post')) {
            $order = am($this->Session->read('Order'), $this->request->data['Order']);

            $treatment = $this->Order->Treatment->find('first', array(
                'conditions' => array(
                    'Treatment.wash' => $order['wash'],
                    'Treatment.antibiotic' => $order['antibiotic'],
                    'Treatment.retinoid' => $order['retinoid'],
                    'Treatment.acne_class_id' => $order['acne_class_id'],
                ),
                'order' => 'Treatment.priority'
            ));

            if (count($treatment) == 0) {
                $this->Session->setFlash(__('If you only want to use benzoyl peroxide or salicyclic acid,
                    no prescriptions from Clear Concierge are necessary! Although we would love to have you as a patient,
                    you can pick these medications up as creams or washes from most local pharmacies, without signing up with us.
                    Please select additional medications if you wish to continue.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger col-xs-12'
                ));
                return $this->redirect(array('action' => 'personalize'));
            }

            $order['treatment_id'] = $treatment['Treatment']['id'];
            $order['status'] = 'consent';

            $this->Session->write('Order', $order);

            //write current user's choice
            $this->Session->write('Choice.wash', $this->request->data['Order']['wash']);
            $this->Session->write('Choice.antibiotic', $this->request->data['Order']['antibiotic']);
            $this->Session->write('Choice.retinoid', $this->request->data['Order']['retinoid']);
            $this->Session->write('Choice.not_allergic', $this->request->data['Order']['not_allergic']);
            $this->Session->write('Choice.doxy_no_condition', $this->request->data['Order']['doxy_no_condition']);
            $this->Session->write('Choice.doxy_no_medication', $this->request->data['Order']['doxy_no_medication']);

            if (!$this->current_user) {
                return $this->redirect(array('controller' => 'users', 'action' => 'add'));
            } else {
                $current_user = $this->current_user;

                if ($this->Session->read('Patient') && $this->Session->read('Order') && $this->current_user) {
                    $provider = $this->User->getProvider();
                    $patient = $current_user['Patient'];
                    $order['user_id'] = (integer)$current_user['User']['id'];
                    $order['patient_id'] = (integer)$patient['id'];
                    $order['provider_id'] = (integer)$provider['User']['id'];
                    $order = $this->Order->save($order);

                    $this->Session->write('Order', $order['Order']);
                }

                return $this->redirect(array('action' => 'consent'));
            }
        }

        // determine acne class
        $this->loadModel('AcneClass');
        $acne = $this->_getOrderAcne();

        $acne_class = $this->AcneClass->determineAcneClass($acne, !$this->Session->read('Order.not_pregnant'));

        $this->Session->write('Order.acne_class_id', $acne_class['id']);

        $treatments = $this->Order->Treatment->find('all', array(
            'conditions' => array('Treatment.acne_class_id' => $acne_class['id']),
            'order' => 'Treatment.priority',
            'contain' => array('Wash', 'Antibiotic', 'Retinoid')
        ));

        // default treatment has priority = 0, ordering by priority
        $recommended = $treatments[0];

        // get alternative meds
        // construction "$wash[$treatment['Wash']['id']]" used for exclude duplicate meds
        foreach ($treatments as $treatment) {
            if ($recommended['Wash']['id'] != $treatment['Wash']['id'] && $treatment['Wash']['id'])
                $wash[$treatment['Wash']['id']] = $treatment['Wash'];
            if ($recommended['Antibiotic']['id'] != $treatment['Antibiotic']['id'] && $treatment['Antibiotic']['id'])
                $antibiotic[$treatment['Antibiotic']['id']] = $treatment['Antibiotic'];
            if ($recommended['Retinoid']['id'] != $treatment['Retinoid']['id'] && $treatment['Retinoid']['id'])
                $retinoid[$treatment['Retinoid']['id']] = $treatment['Retinoid'];
        }
        $acne_class = $acne_class['id'];
        $this->set(compact('acne_class', 'recommended', 'wash', 'antibiotic', 'retinoid', 'user_choice'));
    }

    /*
    * preliminary_fine_aging method
    */
//    public function preliminary_fine_aging_old() {
//
//        $this->Session->write('current_action', 'preliminary_fine_aging');
//        $this->Session->write('current_controller', 'orders');
//
//        /**
//         * If we have an open order, redirect to the next step
//         */
//        if(isset($current_user) && $this->Session->check('Order.id') && $this->Csc->getNextOrderStep()) {
//            return $this->redirect(array('controller' => 'orders', 'action' => $this->Csc->getNextOrderStep()));
//        }
//
//        if ($this->request->is('post')) {
//
//            //for FAL, mild skin darkening
//            if( $this->request->data['Order']['not_allergic'] == 1 &&  $this->request->data['Order']['accept_tretinoin'] == 1) {
//                $this->Session->write('Order.not_allergic', true);
//                $this->Session->write('Order.accept_tretinoin', true);
//                $this->Session->write('Order.status', 'add');
//
//                if(!$this->current_user) {
//                    $this->redirect(array('controller' => 'users', 'action' => 'add'));
//                } else {
//                    $this->redirect(array('action' => 'consent'));
//                }
//            //for eyelash fullness
//            } else if( $this->request->data['Order']['not_allergic'] == 1 &&  $this->request->data['Order']['accept_permanent_pigmentation'] == 1 &&  $this->request->data['Order']['accept_latisse_consequence'] == 1) {
//                $this->Session->write('Order.not_allergic', true);
//                $this->Session->write('Order.accept_permanent_pigmentation', true);
//                $this->Session->write('Order.accept_latisse_consequence', true);
//                $this->Session->write('Order.status', 'add');
//
//                if(!$this->current_user) {
//                    $this->redirect(array('controller' => 'users', 'action' => 'add'));
//                } else {
//                    $this->redirect(array('action' => 'consent'));
//                }
//            }else {
//                $this->Session->setFlash(__('You must accept all condition.'), 'alert', array(
//                    'plugin' => 'BoostCake',
//                    'class' => 'alert-danger'
//                ));
//            }
//        }
//
//        $fine_age = $this->Session->read('Order.fine_age');
//        $order_type = $this->Session->read('Order.type');
//        $this->set(compact('fine_age','order_type'));
//    }


    /*
* preliminary_fine_aging method_modern
*/
    public function preliminary_fine_aging()
    {

        $this->layout = 'modern_layout';
        $this->Session->write('current_action', 'preliminary_fine_aging');
        $this->Session->write('current_controller', 'orders');

        /**
         * If we have an open order, redirect to the next step
         */
        if (isset($current_user) && $this->Session->check('Order.id') && $this->Csc->getNextOrderStep()) {
            return $this->redirect(array('controller' => 'orders', 'action' => $this->Csc->getNextOrderStep()));
        }

        if ($this->request->is('post')) {
            $this->Session->write('Order.user_note', $this->request->data['Order']['user_note']);
            //FOR FAL_EF, MSD_EF
            if (($this->request->data['Order']['not_allergic'] == 1) && ($this->request->data['Order']['accept_tretinoin'] == 1) && ($this->request->data['Order']['accept_permanent_pigmentation'] == 1) && ($this->request->data['Order']['accept_latisse_consequence'] == 1)) {
                $this->Session->write('Order.not_allergic', true);
                $this->Session->write('Order.accept_tretinoin', true);
                $this->Session->write('Order.accept_permanent_pigmentation', true);
                $this->Session->write('Order.accept_latisse_consequence', true);
                $this->Session->write('Order.status', 'add');

                if (!$this->current_user) {
                    $this->redirect(array('controller' => 'users', 'action' => 'add'));
                } else {

                    $provider = $this->User->getProvider();
                    $patient = $this->current_user['Patient'];
                    $this->Session->write('Order.patient_id', (integer)$patient['id']);
                    $this->Session->write('Order.provider_id', (integer)$provider['User']['id']);
                    $this->redirect(array('action' => 'consent'));
                }
            }
            //for FAL, mild skin darkening
            if ($this->request->data['Order']['not_allergic'] == 1 && $this->request->data['Order']['accept_tretinoin'] == 1) {
                $this->Session->write('Order.not_allergic', true);
                $this->Session->write('Order.accept_tretinoin', true);
                $this->Session->write('Order.status', 'add');

                if (!$this->current_user) {
                    $this->redirect(array('controller' => 'users', 'action' => 'add'));
                } else {

                    $provider = $this->User->getProvider();
                    $patient = $this->current_user['Patient'];
                    $this->Session->write('Order.patient_id', (integer)$patient['id']);
                    $this->Session->write('Order.provider_id', (integer)$provider['User']['id']);
                    $this->redirect(array('action' => 'consent'));
                }
                //for eyelash fullness
            } else if ($this->request->data['Order']['not_allergic'] == 1 && $this->request->data['Order']['accept_permanent_pigmentation'] == 1 && $this->request->data['Order']['accept_latisse_consequence'] == 1) {
                $this->Session->write('Order.not_allergic', true);
                $this->Session->write('Order.accept_permanent_pigmentation', true);
                $this->Session->write('Order.accept_latisse_consequence', true);
                $this->Session->write('Order.status', 'add');

                if (!$this->current_user) {
                    $this->redirect(array('controller' => 'users', 'action' => 'add'));
                } else {
                    $provider = $this->User->getProvider();
                    $patient = $this->current_user['Patient'];
                    $this->Session->write('Order.patient_id', (integer)$patient['id']);
                    $this->Session->write('Order.provider_id', (integer)$provider['User']['id']);

                    $this->redirect(array('action' => 'consent'));
                }
            } else {
                $this->Session->setFlash(__('You must accept all condition.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger custom'
                ));
            }
        }

        $fine_age = $this->Session->read('Order.fine_age');
        $order_type = $this->Session->read('Order.type');
        $this->set(compact('fine_age', 'order_type'));
    }

    /*
    * terms method
    * @deprecated
    * @todo I don't think this is used. I think it was replaced by consent.
    * @return void
    */
    public function terms()
    {

        if (!$this->current_user || !$this->Session->read('Order.id')) {
            return $this->redirect(array('action' => 'identify'));
        }
        $accepted['Order']['terms'] = 1;
        $accepted['Order']['id'] = $this->Session->read('Order.id');
        $this->Session->write('Order.status', 'consent');
        if ($this->Order->save($accepted)) {
            $this->redirect(array('action' => 'consent'));
        }
    }


    /*
    * consent method
    *
    * @return void
//    */
//    public function consent_old() {
//
//        $this->Session->write('current_action', 'consent');
//        $this->Session->write('current_controller', 'orders');
//
//        if(!$this->current_user && !$this->Session->read('Order.id')) {
//            $this->redirect(array('action' => 'identify'));
//        }
//
//		// validate consent TODO: validate form with Model validate functional
//        if ($this->Order->checkConsent(($this->request->is('post')) ? $this->request->data['Order'] : null, $this->current_user))  {
//			$this->Session->write('Order.consent', 1);
//
//            $order_src = $this->Session->read('Order');
//            $order_src['consent'] = (int) true;
//            $order_src['status'] =  'consent';//$this->Session->read('Order.status');
//
//			if ($order = $this->Order->save($order_src)) {
//                $this->Session->write('Order.id', $order['Order']['id']);
//				$this->Session->write('Order.status', 'review_start');
//                //always show consent page
//                $count_order = $this->Order->find('count',array(
//                    'conditions' => array('Order.user_id' => $this->Auth->user('id'))
//                ));
//                //if user create first order - don't show consent page twice
//                if($count_order == 1) {
//                    $this->redirect(array('action' => 'review_start'));
//                }
//			} else {
//				$this->Session->setFlash(__('There was a problem verifying your consent. Please try again.'), 'alert', array(
//					'plugin' => 'BoostCake',
//					'class' => 'alert-danger'
//				));
//			}
//		} elseif ($this->request->is('post')) {
//			$this->Session->setFlash(__('You must accept the consent and fill out your full name to continue.'), 'alert', array(
//				'plugin' => 'BoostCake',
//				'class' => 'alert-danger'
//			));
//		}
//
//        $isFLA = $this->Session->read('Order.fine_age');
//        $order_type = $this->Session->read('Order.type');
//
//        $this->set('full_name', $this->current_user['User']['first_name'] . ' ' . ($this->current_user['User']['middle_name'] != '' ? $this->current_user['User']['middle_name'] . ' ' : '') . $this->current_user['User']['last_name']);
//        $this->set('consent_done', $this->Session->read('Order.consent'));
//        $this->set('isFLA', $isFLA);
//        $this->set('order_type', $order_type);
//    }

    /*
* consent_modern method
*
* @return void
*/
    public function consent()
    {

        $this->layout = "modern_layout";
        $this->Session->write('current_action', 'consent');
        $this->Session->write('current_controller', 'orders');

        if (!$this->current_user && !$this->Session->read('Order.id')) {
            $this->redirect(array('action' => 'identify'));
        }


        // validate consent TODO: validate form with Model validate functional
        if ($this->Order->checkConsent(($this->request->is('post')) ? $this->request->data['Order'] : null, $this->current_user)) {
            $this->Session->write('Order.consent', 1);

            $order_src = $this->Session->read('Order');
            $order_src['consent'] = (int)true;
            $order_src['status'] = 'consent';//$this->Session->read('Order.status');
            $order_src['date'] = date("Y-m-d");

            if ($order = $this->Order->save($order_src)) {
                $this->Session->write('Order.id', $order['Order']['id']);
                $this->Session->write('Order.status', 'review_start');
                //always show consent page
                $count_order = $this->Order->find('count', array(
                    'conditions' => array('Order.user_id' => $this->Auth->user('id'))
                ));
                //if user create first order - don't show consent page twice
                if ($count_order == 1) {
                    $this->redirect(array('action' => 'review_start'));
                }
            } else {
                $this->Session->setFlash(__('There was a problem verifying your consent. Please try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger custom'
                ));
            }
        } elseif ($this->request->is('post')) {
            $this->Session->setFlash(__('You must accept the consent and fill out your full name to continue.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-danger custom'
            ));
        }

        $isFLA = $this->Session->read('Order.fine_age');
        $order_type = $this->Session->read('Order.type');

        $this->set('full_name', $this->current_user['User']['first_name'] . ' ' . ($this->current_user['User']['middle_name'] != '' ? $this->current_user['User']['middle_name'] . ' ' : '') . $this->current_user['User']['last_name']);
        $this->set('consent_done', $this->Session->read('Order.consent'));
        $this->set('isFLA', $isFLA);
        $this->set('order_type', $order_type);
        $this->set('order_src', $this->Session->read('Order'));
        $user = $this->User->find('first', ['conditions' => ['User.id' => $this->Auth->user('id')]]);
        $this->set('user', $user);

        $order = $this->Order->find('first', array(
            'conditions' => array('Order.id' => $this->Session->read('Order.id'))
        ));
        //$data = array('date' => date("Y-m-d"));
        //$this->Order->set($data);
        //$this->Order->save();
        $this->Session->delete('Choice');
    }

    /*
    * review_start method
    *
    * @return void
    */
//    public function review_start_old() {
//
//        $this->Session->write('current_controller', 'orders');
//        $this->Session->write('current_action', 'review_start');
//
//        $this->Session->write('Order.status', 'review_start');
//
//        $order_src = $this->Session->read('Order');
//        $this->Order->save($order_src);
//
//        if(!$this->current_user && !$this->Session->read('Order.id')) {
//            return $this->redirect(array('action' => 'identify'));
//        }
//
//        $this->set('isFLA', $this->Session->read('Order.fine_age'));
//    }

    /*
* review_start method
*
* @return void
*/
    public function review_start()
    {

        $this->layout = "modern_layout";
        $this->Session->write('current_controller', 'orders');
        $this->Session->write('current_action', 'review_start');

        $this->Session->write('Order.status', 'review_start');

        $get_email = $this->current_user['User']['email_consent'];

        $order['Order']['get_email'] = $get_email;
        $this->Session->write('Order.get_email', $this->current_user['User']['email_consent']);

        $order_src = $this->Session->read('Order');

        //$prices = array('fal' => 65, 'msd' => 85, 'ef' => 120, 'fal_ef' => 185, 'msd_ef' => 205, 'acne' => 29);
        $prices = array('fal' => 29, 'msd' => 29, 'ef' => 29, 'fal_ef' => 29, 'msd_ef' => 29, 'acne' => 29);
        foreach ($prices as $type => $price) {
            if ($type == $order_src['type']) {
                $order_src['order_cost'] = $price;
            }
        }

        if (!$this->current_user && !$this->Session->read('Order.id')) {
            return $this->redirect(array('action' => 'identify'));
        }

        $referral_token = $this->Session->read('Referral.token');
        if (!is_null($referral_token)) {
            $this->loadModel('Referral');
            $owner = $this->User->find('first', array(
                'conditions' => array('referral_token' => $referral_token)
            ));
            $referral['Referral']['owner_id'] = $owner['User']['id'];
            $referral['Referral']['user_id'] = $this->Auth->user('id');
            //$referral['Referral']['token'] = $referral_token;
            $referral['Referral']['created'] = date("Y-m-d H:i:s");
            $this->Referral->create();
            $this->Referral->save($referral['Referral']);
            $user = $this->User->find('first', array(
                'conditions' => array('User.id' => $this->Auth->user('id'))
            ));
            $data = array('id' => $this->Auth->user('id'), 'balance' => 5);
            $this->User->save($data);
        }
        $this->Order->id = $this->Session->read('Order.id');

        $order_src["consent"] = 1;
        //$this->Order->set('consent', 1);

        $this->Order->save($order_src);
        //$this->Order->save();
        $this->set('isFLA', $this->Session->read('Order.fine_age'));

        $this->set('current_user', $order_src);
        $this->set('order', $order_src);
    }



//    public function stripe_old($order_id = null, $promo_code = null)
//	{
//        $this->loadModel('PromoCode');
//        $discount = $this->PromoCode->getDiscount($promo_code);
//
//        //save in order new cost
//        if($discount == 0){
//            $this->Session->write('paid', 1);
//            $this->Order->updateAll(
//                array('Order.order_cost' => $discount),
//                array('Order.user_id' => $this->Auth->user('id'),
//                array('Order.id' => $order_id)),
//                array('Order.paid' => 1)
//            );
//            $this->redirect(array('action' => 'patient'));
//            return;
//        }else{
//            $this->Order->updateAll(
//                array('Order.order_cost' => $discount),
//                array('Order.user_id' => $this->Auth->user('id'),
//                array('Order.id' => $order_id))
//            );
//        }
//
//        $this->set('order_id', $order_id);
//        $this->set('discount', $discount);
//        $this->set('user_id', $this->Auth->user('id'));
//    }

    public function stripe($order_id = null, $promo_code = null)
    {
        $this->layout = "modern_layout";
        $this->loadModel('PromoCode');
        $this->loadModel('User');
        if ($this->request->params["named"]["response"]) {
            $this->set('response', $this->request->params["named"]["response"]);
        }

        $discount = $this->PromoCode->getDiscount($promo_code);
        $user = $this->User->find('first', ['conditions' => ['User.id' => $this->Auth->user('id')]]);
        $order = $this->Order->find('first', ['conditions' => ['Order.id' => $order_id]]);
        //save in order new cost
        if (intVal($order["Order"]["order_cost"]) == 0 && $order["Order"]["transaction_type"] == "promo_code") {

            $this->Order->id = $order["Order"]["id"];
            $this->Order->set('paid', 1);
            $this->Order->save();
            $this->redirect(array('action' => 'patient'));
            return;
        } elseif (intVal($order["Order"]["order_cost"]) == 0) {
            $this->Order->id = $order["Order"]["id"];
            $this->Order->set('order_cost', 29);
            $this->Order->save();
        }

        if ($discount == 0) {
            $this->Session->write('paid', 1);
            if (CakeSession::read('PromoCode.current_code') == 'exists') {
                $this->Order->id = $order_id;
                $this->Order->setFreePaypal($order_id, $this->Auth->user('id'));
            } else {
                $this->Order->updateAll(
                    array('Order.order_cost' => $discount),
                    array('Order.user_id' => $this->Auth->user('id'),
                        array('Order.id' => $order_id)),
                    array('Order.paid' => 1),
                    array('Order.transaction_type' => 'credit_for_referral')
                );
            }
            $this->redirect(array('action' => 'patient'));
            return;
        } else {
            if ($user['User']['balance'] > $this->Session->read('Order.order_cost') && $discount < 20) {
                $this->Session->write('paid', 1);
                $this->Order->updateAll(
                    array('Order.order_cost' => $discount),
                    array('Order.user_id' => $user['User']['id'],
                        array('Order.id' => $order_id)),
                    array('Order.paid' => 1),
                    array('Order.transaction_type' => 'credit_for_referral')
                );
                //CakeSession::write('PromoCode.current_code', $promo_code);
                $this->User->id = $user['User']['id'];
                $this->User->set('balance', ($user['User']['balance'] - $this->Session->read('Order.order_cost')));
                $this->User->save();
                $this->redirect(array('action' => 'patient'));
                return;
            } else if ($user['User']['balance'] > 0 && $this->Session->read('Order.type') == 'acne' && $discount > 20) {
                $discount = $this->Order->getOrderCost() - $user['User']['balance'];

                $this->Order->updateAll(
                    array('Order.order_cost' => $discount),
                    array('Order.user_id' => $user['User']['id'],
                        array('Order.id' => $order_id))
                );
            } else if ($user['User']['balance'] > 0 && $this->Session->read('Order.type') != 'acne' && $discount > 20) {
                $discount = $this->Session->read('Order.order_cost') - $user['User']['balance'];
                $this->Order->updateAll(
                    array('Order.order_cost' => $discount),
                    array('Order.user_id' => $user['User']['id'],
                        array('Order.id' => $order_id))
                );
            } else if ($this->Session->read('Order.type') != 'acne' && $discount > 20) {
                $discount = $this->Session->read('Order.order_cost');
            } else {
                $this->Order->updateAll(
                    array('Order.order_cost' => $discount),
                    array('Order.user_id' => $user['User']['id'],
                        array('Order.id' => $order_id))
                );
            }
        }

        $this->set('order_id', $order_id);
        $this->set('discount', $discount);
        $this->set('user_id', $this->Auth->user('id'));
        $this->set('user', $user);

    }

    public function charge($id = null)
    {
        $token = $_POST['stripeToken'];
        $this->loadModel('User');
        $user = $this->User->find('first', ['conditions' => ['User.id' => $this->Auth->user('id')]]);

        $this->Order->recursive = 0;
        if (!$this->Order->read(null, $id)) {
            throw new Exception('Order not found: ' . $id);
        }

        //get order cost after enter promo code
        $order_cost = $this->Order->find('first', array(
            'conditions' => array(
                'Order.user_id' => $this->Auth->user('id'),
                'Order.id' => $id
            )
        ));

        if (count($order_cost)) {
            $price = (float)$order_cost['Order']['order_cost'];
        } else {
            $price = 29;
        }

        $result = $this->Stripe->charge(array(
            'amount' => $price,
            'stripeToken' => $token,
        ));

        if (empty($result['stripe_id'])) {
            $this->redirect(array('action' => 'stripe/502', $id, 'admins' => false));
            return;
        }

        if ($user['User']['balance'] > 0) {
            $this->loadModel('Referral');
            if ($this->Referral->checkPaidReferral($user['User']['id'])) {
                $transaction_type = 'credit_for_referral';
            } else {
                $transaction_type = 'referral';
            }
        } else {
            if (CakeSession::read('PromoCode.current_code') == 'exists') {
                $transaction_type = 'promo_code';
            } else {
                $transaction_type = 'new';
            }
        }

        $isExpired = $this->Order->isExpired();
        if ($this->Order->paid($result['stripe_id'], true, $price, $transaction_type)) {

            $this->redirect(array('action' => 'received', $id, $isExpired));
        } else {
            throw new Exception('Payment save false for order: ' . $id);
        }
    }

    public function charge_modern($id = null)
    {
        $token = $_POST['stripeToken'];
        $this->loadModel('User');
        $id = $_POST['order_id'];
        //$this->loadModel('User');
        $user = $this->User->find('first', ['conditions' => ['User.id' => $this->Auth->user('id')]]);

        $this->Order->recursive = 0;
        if (!$this->Order->read(null, $id)) {
            throw new Exception('Order not found: ' . $id);
        }

        //get order cost after enter promo code
        $order_cost = $this->Order->find('first', array(
            'conditions' => array(
                'Order.user_id' => $this->Auth->user('id'),
                'Order.id' => $id
            )
        ));

        if (count($order_cost)) {
            $price = (float)$order_cost['Order']['order_cost'];
        } else {
            $price = 29;
        }

        $result = $this->Stripe->charge(array(
            'amount' => $price,
            'stripeToken' => $token,
        ));
        CakeLog::write('debug', '$result' . print_r($result, true));
        if (empty($result['stripe_id'])) {
            $this->redirect(array('action' => 'stripe', $id, 'admins' => false, 'response' => "We're sorry, your card was declined. Please try again in case there was entry error. If you continue having problems, leave us a message at info@clearskinconcierge.com and we'll check up on this for you."));
            //$this->redirect(array('action' => 'stripe'));
            return;
        }

        if ($user['User']['balance'] > 0) {
            $this->loadModel('Referral');
            if ($this->Referral->checkPaidReferral($user['User']['id'])) {
                $transaction_type = 'credit_for_referral';
            } else {
                $transaction_type = 'referral';
            }
        } else {
            if (CakeSession::read('PromoCode.current_code') == 'exists') {
                $transaction_type = 'promo_code';
            } else {
                $transaction_type = 'new';
            }
        }

        $isExpired = $this->Order->isExpired();
        if ($this->Order->paid($result['stripe_id'], true, $price, $transaction_type)) {
            $this->loadModel('User');
            $user = $this->User->find('first', ['conditions' => ['User.id' => $this->Auth->user('id')]]);
            $this->User->id = $user['User']['id'];
            $this->User->set('balance', 0);
            $this->User->save();
            //referral bonus
            $this->loadModel('Referral');
            $referral = $this->Referral->find('first', [
                'conditions' => ['Referral.user_id' => $this->Auth->user('id'), 'Referral.paid' => 0],
                'limit' => 1
            ]);
            if ($referral) {
                $this->User->id = $referral['User']['id'];
                $this->User->set('balance', ($referral['User']['balance'] + Configure::read('Referral.balance')));
                $this->User->save();
                $this->Referral->id = $referral['Referral']['id'];
                $this->Referral->set('paid', 1);
                $this->Referral->save();
            }
            //end referral bonus
            $this->redirect(array('action' => 'received_modern', $id, $isExpired));
        } else {
            throw new Exception('Payment save false for order: ' . $id);
        }
    }

    /**
     * payment method
     *
     * @param $id int|null order id
     */
//	public function payment_old($id)
//	{
//        $this->set('nav_step', 4);
//        if(!$this->current_user) {
//            return $this->redirect(array('action' => 'identify'));
//        }
//
//		$this->Order->recursive = 0;
//
//		$id = ($id) ?: $this->Session->read('Order.id');
//		$order = $this->Order->findById($id);
//
//		// Is this the order that is processed (saved in session)
//		$isCurrent = $this->Order->isCurrent($order);
//
//
//		// Validate order owner
//		$validateOwner = ($this->current_user) ?
//			($isCurrent || $this->Order->isOwner($order, $this->current_user['Patient']['id'])) : false;
//
//		if(!$validateOwner || !$order) {
//            return $this->redirect(array('action' => 'identify'));
//        }
//
//		$isExpired = $this->Order->isExpired($order);
//
//		if ($isCurrent && !$isExpired) {
//			$this->Session->write('current_controller', 'orders');
//			$this->Session->write('current_action', 'payment');
//            $this->Session->write('Order.status', 'payment');
//
//            $order_src = $this->Session->read('Order');
//            $this->Order->save($order_src);
//
//			$this->renewalSessionData($order);
//			if ($this->redirectToCompleteOrder($order)) {
//				return;
//			}
//
//			$this->paymentSkipMode($order['Order']['id']);
//		}
//
//        $fine_age = $order['Order']['fine_age'];
//        $payment_error = $this->Session->read('payment_error');
//
//        $this->set(compact('order','fine_age','payment_error', 'isExpired'));
//    }

    /**
     * payment method
     *
     * @param $id int|null order id
     */

    public function payment($id)
    {
        $this->layout = "modern_layout";
        $this->set('nav_step', 4);
        if ($this->request->params["named"]["response"]) {
            $this->set('response', "This transaction couldn't be completed. Please check your Paypal account and balance or try again.");
        }
        if (!$this->current_user) {
            return $this->redirect(array('action' => 'identify'));
        }
        CakeSession::write('PromoCode.current_code', 'not_exists');

        $this->Order->recursive = 0;

        //$id = ($id) ?: $this->Session->read('Order.id');
        $order = $this->Order->findLastPatientOrder($this->current_user['Patient']['id']);

        // Is this the order that is processed (saved in session)
        $isCurrent = $this->Order->isCurrent($order);

        // Validate order owner
        $validateOwner = ($this->current_user) ?
            ($isCurrent || $this->Order->isOwner($order, $this->current_user['Patient']['id'])) : false;

        if (!$validateOwner || !$order) {
            return $this->redirect(array('action' => 'identify'));
        }

        $isExpired = $this->Order->isExpired($order);

        if ($isCurrent && !$isExpired) {
            $this->Session->write('current_controller', 'orders');
            $this->Session->write('current_action', 'payment');
            $this->Session->write('Order.status', 'payment');

            $order_src = $this->Session->read('Order');
            $order_src["consent"] = 1;
            $this->Order->save($order_src);

            $this->renewalSessionData($order);
            if ($this->redirectToCompleteOrder($order)) {
                return;
            }

            $this->paymentSkipMode($order['Order']['id']);
        }
        $this->Session->write('Order.order_cost', $order['Order']['order_cost']);
        $fine_age = $order['Order']['fine_age'];
        $payment_error = $this->Session->read('payment_error');
        $this->loadModel('User');

        $user = $this->User->find('first', ['conditions' => ['User.id' => $this->Auth->user('id')]]);
        $this->set(compact('order', 'fine_age', 'payment_error', 'isExpired', 'user'));
    }

    /**
     * Legacy code. Probably for fix data corruption in database and session
     * My opinion: store data in session is bad way for this task
     * TODO: store data in one storage at the same time at least
     *
     * array $order order data in default array format from model
     */
    protected function renewalSessionData($order)
    {
        $this->Session->write('Order.acne_forehead', $order['Order']['acne_forehead']);
        $this->Session->write('Order.acne_cheeks', $order['Order']['acne_cheeks']);
        $this->Session->write('Order.acne_chin', $order['Order']['acne_chin']);
        $this->Session->write('Order.acne_chest', $order['Order']['acne_chest']);
        $this->Session->write('Order.acne_back', $order['Order']['acne_back']);
        $this->Session->write('Order.acne_neck', $order['Order']['acne_neck']);
        $this->Session->write('Order.acne_nose', $order['Order']['acne_nose']);
        $this->Session->write('Order.acne_temple', $order['Order']['acne_temple']);
        $this->Session->write('Order.fine_age', $order['Order']['fine_age']);
        $this->Session->write('Order.accept_tretinoin', $order['Order']['accept_tretinoin']);
        $this->Session->write('Order.short', $order['Order']['short']);
        $this->Session->write('Order.consent', $order['Order']['consent']);
        $this->Session->write('Order.get_email', $order['Order']['get_email']);
        $this->Session->write('Order.order_cost', $order['Order']['order_cost']);
    }

    /**
     * Legacy code. Probably for redirect to unfilled step of order.
     * My opinion: this case is impossible in current logic.
     * TODO: check and delete if assumptions is true
     *
     * @param array $order order data in default array format from model
     *
     * @return bool
     */
    protected function redirectToCompleteOrder($order)
    {
        if ($order['Order']['complete'] == 1 && $order['Order']['paid'] == 1 && $order['Order']['paypal_code'] != "") {
            $this->Session->write('paid', 1);
            $this->Session->write('complete', 1);
            $this->Session->write('Order.status', 'overview');
            $this->redirect(array('action' => 'overview'));

            return true;
        }

        if ($order['Order']['terms'] == 0) {
            $this->Session->write('Order.status', 'terms');
            $this->redirect(array('action' => 'terms'));

            return true;
        }

        if ($order['Order']['consent'] == 0) {
            $this->Session->write('Order.status', 'consent');
            $this->redirect(array('action' => 'consent'));

            return true;
        }

        if ($order['Order']['paid'] == 1 && $order['Order']['paypal_code'] != "") {
            $this->Session->write('paid', 1);
            $this->Session->write('Order.status', 'patient');
            $this->redirect(array('action' => 'patient'));

            return true;
        }

        return false;
    }

    /**
     * Legacy code. Probably for skip payment steps.
     * My opinion: this debug opportunity not work correct in current logic.
     * TODO: check and delete or repair if assumptions is true
     *
     * @param int $id order id
     *
     * @return bool
     */
    protected function paymentSkipMode($id)
    {
        if (!$this->cscsettings['skipmode']) {
            return false;
        }
        $this->Order->set('id', $id);
        if (!$this->Order->paid('SKIP123')) {
            return false;
        }

        $this->Session->write('paid', 1);
        return true;
    }

    /**
     * received method (redirect from PayPal and Stripe)
     *
     * @param $id null|false order id
     * @param $renew bool is order has been renewal
     */
    public function received($id = null, $renew = false)
    {
        $id = ($id) ?: $this->Session->read('Order.id');
        $order = $this->Order->findById($id);

        // Is this the order that is processed (saved in session)
        $isCurrent = $this->Order->isCurrent($order);

        // Validate order owner
        $validateOwner = ($this->current_user) ?
            ($isCurrent || $this->Order->isOwner($order, $this->current_user['Patient']['id'])) : false;

        if (!$validateOwner || !$order) {
            return $this->redirect(array('action' => 'identify'));
        }

        if (!$this->current_user) {
            $this->redirect(array('action' => 'identify'));
            return;
        }

        if ($this->Order->isPaid($order) && !$this->Order->isExpired($order)) {
            $this->redirect(array('action' => 'patient'));
            return;
        }

        if ($isCurrent && !$renew) {
            $this->Session->write('current_controller', 'orders');
            $this->Session->write('current_action', 'received');
            $this->Session->write('payment_error', 1);
        }

        $this->set(compact('order', 'renew'));
    }


    /**
     * received method (redirect from PayPal and Stripe)
     *
     * @param $id null|false order id
     * @param $renew bool is order has been renewal
     */
    public function received_modern($id = null, $renew = false)
    {
        //$id = ($id) ?: $this->Session->read('Order.id');
        //$order = $this->Order->findById($id);
        $order = $this->Order->findLastPatientOrder($this->current_user['Patient']['id']);

        // Is this the order that is processed (saved in session)
        $isCurrent = $this->Order->isCurrent($order);

        // Validate order owner
        $validateOwner = ($this->current_user) ?
            ($isCurrent || $this->Order->isOwner($order, $this->current_user['Patient']['id'])) : false;

        if (!$validateOwner || !$order) {
            return $this->redirect(array('action' => 'identify'));
        }

        if (!$this->current_user) {
            $this->redirect(array('action' => 'identify'));
            return;
        }

        if ($this->Order->isPaid($order) && !$this->Order->isExpired($order)) {
            $this->Session->write('paid', 1);
            $this->redirect(array('action' => 'patient'));
            return;
        }

        if ($isCurrent && !$renew) {
            $this->Session->write('current_controller', 'orders');
            $this->Session->write('current_action', 'received_modern');
            $this->Session->write('payment_error', 1);
        }

        $this->set(compact('order', 'renew'));
    }


    /*
     * patient method
    * Creates the patient record.
    * @return void
    */
//    public function patient_old() {
//        $this->Session->write('current_controller', 'orders');
//        $this->Session->write('current_action', 'patient');
//
//        $this->set('nav_step', 4);
//        if(!$this->current_user && !$this->Session->read('Order.id')) {
//            return $this->redirect(array('action' => 'identify'));
//        } else if(!$this->Session->read('paid')) {
//            $this->Order->recursive = 0;
//
//            $order = $this->Order->find('first', array('conditions' => array('Order.id' => $this->Session->read('Order.id'))));
//
//            if($order['Order']['paid'] != 1 || $order['Order']['paypal_code'] == "") {
//                $this->Session->write('Order.status', 'payment');
//                return $this->redirect(array('action' => 'payment'));
//            }
//            else {
//                $this->Session->write('paid', 1);
//            }
//        }
//
//        if ($this->request->is('post') || $this->request->is('put')) {
//            $patient = $this->Order->Patient->find('first', array('conditions' => array('Patient.id' => $this->current_user['Patient']['id'])));
//            if(isset($patient)) {
//                $this->request->data['Patient']['id'] = $patient['Patient']['id'];
//            }
//
//            $dob = explode("-", $this->request->data['Patient']['date_of_birth']);
//            if(isset($dob[2]) && strlen($dob[0]) == 2 && strlen($dob[1]) == 2 && strlen($dob[2]) == 4) {
//                $this->request->data['Patient']['date_of_birth'] = $dob[2] .'-'. $dob[0] .'-'. $dob[1];
//            } else {
//                $this->request->data['Patient']['date_of_birth'] = "";
//            }
//            $provider = $this->Order->Provider->getProvider();
//            $this->request->data['Patient']['provider_id'] = $provider['Provider']['id'];
//            $this->request->data['Patient']['user_id'] = $this->current_user['User']['id'];
//            //Set the Patient Status.
//            //@todo set Patient Status.
//
//            $this->request->data['Patient'] = str_replace(array('&gt;','&lt;'), array('>','<'), $this->request->data['Patient']);
//
//
//            if ($patient = $this->Order->Patient->save($this->request->data)) {
//                $this->Session->write('Order.Patient',$patient['Patient']);
//                $this->Order->id = $this->Session->read('Order.id');
//                $this->Order->saveField('status','upload');
//                $this->Order->saveField('paid',1);
//                $this->Order->saveField('patient_id', $patient['Patient']['id']);
//                $this->Order->saveField('provider_id', $provider['Provider']['id']);
//                $this->Session->write('Order.status', 'upload');
//
//                $this->redirect(array('controller' => 'patient_photos', 'action' => 'upload'));
//            } else {
//                $this->Session->setFlash(__('We could not complete your order, please see below.'), 'alert', array(
//                    'plugin' => 'BoostCake',
//                    'class' => 'alert-danger'
//                ));
//            }
//        } else {
//            $this->request->data = $this->Order->Patient->find('first', array('conditions' => array('Patient.id' => $this->current_user['Patient']['id'])));
//            if(!$this->request->data) {
//                $this->request->data['Patient']['gender'] = "";
//                $this->request->data['Patient']['reminders'] = "";
//                $this->request->data['Patient']['time'] = "";
//                $this->request->data['Patient']['skin_type_id'] = "";
//                $this->request->data['Patient']['photosensitivity'] = 0;
//            }
//
//        }
//
//        if(isset($this->request->data['Patient']['date_of_birth']) && !empty($this->request->data['Patient']['date_of_birth']) ) {
//            $dob = explode("-", $this->request->data['Patient']['date_of_birth']);
//            $this->request->data['Patient']['date_of_birth'] = $dob[1] .'-'. $dob[2] .'-'. $dob[0];
//        }
//
//        $fine_age = $this->Session->read('Order.fine_age');
//        $order_type = $this->Session->read('Order.type');
//
//        $this->set(compact('fine_age','order_type'));
//    }

    /*
 * patient method
* Creates the patient record.
* @return void
*/
    function save_meds_used()
    {
        return $this->data;

    }

    public function patient()
    {

        $this->layout = "modern_layout";
        $this->Session->write('current_controller', 'orders');
        $this->Session->write('current_action', 'patient');
        $this->Session->delete('Referral.token');

        $this->set('nav_step', 4);

        if (!$this->current_user && !$this->Session->read('Order.id')) {
            return $this->redirect(array('action' => 'identify'));
        } else if (!$this->Session->read('paid')) {
            $this->Order->recursive = 0;

            $order = $this->Order->find('first', array('conditions' => array('Order.id' => $this->Session->read('Order.id'))));

            if ($order['Order']['paid'] != 1 || $order['Order']['paypal_code'] == "") {
                $this->Session->write('Order.status', 'payment');
                return $this->redirect(array('action' => 'payment'));
            } else {
                $this->Session->write('paid', 1);
            }
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $patient = $this->Order->Patient->find('first', array('conditions' => array('Patient.id' => $this->current_user['Patient']['id'])));
            if (isset($patient)) {
                $this->request->data['Patient']['id'] = $patient['Patient']['id'];
            }

            $dob = explode("-", $this->request->data['Patient']['date_of_birth']);
            if (isset($dob[2]) && strlen($dob[0]) == 2 && strlen($dob[1]) == 2 && strlen($dob[2]) == 4) {
                $this->request->data['Patient']['date_of_birth'] = $dob[2] . '-' . $dob[0] . '-' . $dob[1];
            } else {
                $this->request->data['Patient']['date_of_birth'] = "";
            }
            $provider = $this->Order->Provider->getProvider();
            $this->request->data['Patient']['provider_id'] = $provider['Provider']['id'];
            $this->request->data['Patient']['user_id'] = $this->current_user['User']['id'];
            //Set the Patient Status.
            //@todo set Patient Status.

            $this->request->data['Patient'] = str_replace(array('&gt;', '&lt;'), array('>', '<'), $this->request->data['Patient']);

            $this->request->data['Patient']['medications_not_interested'] = json_encode($this->request->data["Patient"]["medications_not_interested"]);
            $this->request->data["Patient"]["health_situations"] = json_encode($this->request->data["Patient"]["health_situations"]);
           // $this->request->data["Patient"]["medications"] = json_encode($this->request->data["Patient"]["medications"]);


            $temparray3=$this->request->data;
            $temparray2=$this->request->data["Patient"]["medications"];

            foreach($temparray2 as $k=>$v) {
                $testArray2[] = [$v => $temparray3[$v]];
            }


            if ($patient = $this->Order->Patient->save($this->request->data)) {
                $order_from_db = $this->Order->findLastPatientOrder($patient['Patient']['id']);
                $this->Session->write('Order.Patient', $patient['Patient']);
                //$this->Order->id = $this->Session->read('Order.id');
                $this->Order->id = $order_from_db['Order']['id'];
                $this->Order->saveField('status', 'upload');
                $this->Order->saveField('paid', 1);
                $this->Order->saveField('patient_id', $patient['Patient']['id']);
                $this->Order->saveField('provider_id', $provider['Provider']['id']);
                $this->Session->write('Order.status', 'upload');
                $this->Session->write('selected_pharmacy', $this->request->data["Patient"]["select_pharmacy"]);
                $this->Session->read('selected_pharmacy');
                $this->redirect(array('controller' => 'patient_photos', 'action' => 'upload'));
            } else {
                $this->Session->setFlash(__('We could not complete your order, please see below.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger custom'
                ));
            }
        } else {

            $this->request->data = $this->Order->Patient->find('first', array('conditions' => array('Patient.id' => $this->current_user['Patient']['id'])));
            // if(is_string(CakeSession::read('PromoCode.current_code'))){
            //     $this->Order->id = $this->Session->read('Order.id');
            //     $this->Order->saveField('transaction_type',CakeSession::read('PromoCode.current_code'));
            // }
            if (!$this->request->data) {
                $this->request->data['Patient']['gender'] = "";
                $this->request->data['Patient']['reminders'] = "";
                $this->request->data['Patient']['time'] = "";
                $this->request->data['Patient']['skin_type_id'] = "";
                $this->request->data['Patient']['photosensitivity'] = 0;
            }

        }

        if (isset($this->request->data['Patient']['date_of_birth']) && !empty($this->request->data['Patient']['date_of_birth'])) {
            $dob = explode("-", $this->request->data['Patient']['date_of_birth']);
            $this->request->data['Patient']['date_of_birth'] = $dob[1] . '-' . $dob[2] . '-' . $dob[0];
        }
        $order = $this->Order->find('first', array('conditions' => array('Order.id' => $this->Session->read('Order.id'))));
        $this->set('order', $order);
        $fine_age = $this->Session->read('Order.fine_age');
        $order_type = $this->Session->read('Order.type');
        $this->set(compact('fine_age', 'order_type'));
    }


    /*
    * tips method
    *
    * @return void
    */
    public function tips()
    {
        $this->set('nav_step', 4);
        if (!$this->current_user || !$this->Session->read('Order.id')) {
            return $this->redirect(array('action' => 'identify'));
        } else if (!$this->Session->read('paid')) {
            return $this->redirect(array('action' => 'payment'));
        }
        $fine_age = $this->Session->read('Order.fine_age');
        $this->set(compact('fine_age'));
        $this->set('comments', $this->Session->read('Questionnaire.comments'));
    }

    /*
    * upload method
    *
    * @return void
    */
    public function upload()
    {

        //$this->set('nav_step', 4);
        if (!$this->current_user && !$this->Session->read('Order.id')) {
            return $this->redirect(array('action' => 'identify'));
        }
        /**
         * If we have an open order, and this is not the next step. Redirect to the next step.
         */

        if (isset($this->current_user) && ($this->request->action != $this->Session->read('Order.status'))) {
            return $this->redirect(array('controller' => 'orders', 'action' => $this->Csc->getNextOrderStep()));
        }

        if (isset($this->request->data['Order']['webcamDone'])) {
            //trigger event to notify Provider that photos have been uploaded.
            $order = $this->Order->find('first', array('conditions' => array('Order.id' => $this->Session->read('Order.id'))));
            $this->Order->getEventManager()->dispatch(
                new CakeEvent(
                    'Controller.Order.uploadComplete',
                    $this,
                    array_merge(
                        array('order' => $order),
                        array('user' => $this->current_user),
                        array('passParams' => true)
                    )));

            return $this->redirect(array('action' => 'thanks'));
        }

        if ($this->Session->read('Order.acne_forehead') != '00000' ||
            $this->Session->read('Order.acne_cheeks') != '00000' ||
            $this->Session->read('Order.acne_chin') != '00000' ||
            $this->Session->read('Order.acne_neck') != '00000' ||
            $this->Session->read('Order.acne_nose') != '00000' ||
            $this->Session->read('Order.acne_temple') != '00000')
            $face = true;
        else
            $face = false;
        if ($this->Session->read('Order.acne_chest') != '00000' ||
            $this->Session->read('Order.acne_back') != '00000')
            $body = true;
        else
            $body = false;
        $fine_age = $this->Session->read('Order.fine_age');

        $invalid = false;
        if ($this->request->is('post') || $this->request->is('put')) {
            $valid = array(
                'image/jpeg' => 'image/jpeg',
            );
            $photos = array(
                'face_photo' => 'face_photo',
                'left_photo' => 'left_photo',
                'right_photo' => 'right_photo',
                'chest_photo' => 'chest_photo',
                'back_photo' => 'back_photo'
            );
            foreach ($photos as $photo) {
                if (isset($this->request->data["Order"][$photo]) && !$this->request->data["Order"][$photo]['tmp_name']) {
                    unset($this->request->data["Order"][$photo]);
                }
                if (isset($this->request->data["Order"][$photo]) && !isset($valid[$this->request->data["Order"][$photo]['type']])) {
                    $invalid = true;
                    break;
                }
            }
            if (($face || $fine_age) && (!isset($this->request->data["Order"]['face_photo']) || !isset($this->request->data["Order"]['left_photo']) || !isset($this->request->data["Order"]['right_photo']))) {
                $invalid = true;
            }
            if (($body && !$face) && (!isset($this->request->data["Order"]['chest_photo']) || !isset($this->request->data["Order"]['back_photo']))) {
                $invalid = true;
            }
            if (!$invalid) {
                $folder_url = WWW_ROOT . 'files' . DS . 'images' . DS . $this->current_user['User']['id'] . DS . $this->Session->read('Order.id'); // Set path for image
                //$folder_url = WWW_ROOT . 'files' . DS . 'images' . DS . $this->current_user['Patient']['id']; // Set path for image

                // create the folder if it does not exist
                if (!is_dir($folder_url)) {
                    mkdir($folder_url, 0755, true);
                }

                foreach ($photos as $photo) {
                    if (isset($this->request->data["Order"][$photo])) {
                        $file_ext = strtolower(substr($this->request->data["Order"][$photo]['name'], strrpos($this->request->data["Order"][$photo]['name'], '.')));
                        $filename = $photo . $file_ext;
                        move_uploaded_file($this->request->data["Order"][$photo]['tmp_name'], $folder_url . DS . $filename); // Store file

                        $patientPhoto['patient_id'] = $this->current_user['Patient'];
                        $bodyArea = substr($this->request->data["Order"][$photo]['name'], 0, strlen($this->request->data["Order"][$photo]['name']) - (strchr($this->request->data["Order"][$photo]['name'], '_') + 1));
                        if ($bodyArea == 'left') {
                            $bodyArea = 'left_profile';
                        }
                        if ($bodyArea == 'right') {
                            $bodyArea = 'right_profile';
                        }
                        $patientPhoto['body_area'] = $bodyArea;
                        $patientPhoto['filename'] = $photo . date('Ymd') . $file_ext;
                        $patientPhoto['path'] = 'files' . DS . 'images' . DS . $this->current_user['Patient']['id'];
                        copy($folder_url . DS . $filename, WWW_ROOT . $patientPhoto['path']); // Store file
                        $this->Order->Patient->PatientPhoto->save($photo);
                    }
                }
                $this->Order->getEventManager()->dispatch(
                    new CakeEvent(
                        'Controller.Order.uploadComplete',
                        $this,
                        array_merge(
                            array('order' => $order),
                            array('user' => $this->current_user),
                            array('passParams' => true)
                        )));
                return $this->redirect(array('action' => 'thanks'));
            }
        }

        $order_id = $this->Session->read('Order.id');
        $user_id = $this->current_user['User']['id'];
        $user_name = $this->current_user['User']['first_name'] . " " . ($this->current_user['User']['middle_name'] != "" ? $this->current_user['User']['middle_name'] . " " : "") . $this->current_user['User']['last_name'];

        $this->set(compact('order_id', 'user_id', 'user_name', 'invalid', 'fine_age', 'face', 'body'));
    }

    public function save_webcam_photo()
    {
        $this->layout = false;
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $this->log($this->request->data);
            $this->log($this->current_user);
            $type = $this->request->data('photoType');
            $userId = $this->request->data('userId');
            $patientId = $this->current_user['Patient']['id'];
            $orderId = $this->request->data('orderId');
            $img = str_replace('data:image/jpeg;base64,', '', $this->request->data('base64'));
            $img = str_replace(' ', '+', $img);

            $imageData = base64_decode($img);
            $filePath = 'files' . DS . 'images' . DS . $patientId . DS . $type . '_' . date('Ymd') . '.jpg';
            //$this->log($filePath);
            $patientPhoto['patient_id'] = $this->current_user['Patient']['id'];
            $bodyArea = strstr($type, '_', true);
            if ($bodyArea == 'left') {
                $bodyArea = 'left_profile';
            }
            if ($bodyArea == 'right') {
                $bodyArea = 'right_profile';
            }
            $patientPhoto['body_area'] = $bodyArea;
            $patientPhoto['filename'] = $type . '_' . date('Ymd') . '.jpg';
            $patientPhoto['path'] = 'files' . DS . 'images' . DS . $patientPhoto['patient_id'] . DS;
            //copy($folder_url . DS . $filename,WWW_ROOT . $patientPhoto['path']); // Store file
            $this->log($patientPhoto);
            $this->Order->Patient->PatientPhoto->save($patientPhoto);
            $file = new File(WWW_ROOT . DS . $filePath, true);

            if ($file->write($imageData)) {
                return $filePath;
            }

        }
    }

    /*
    * thanks method
    *
    * @return void
    */
//    public function thanks_old() {
//
//        $this->Session->delete('current_action');
//        $this->Session->delete('current_controller');
//
//        $orderData['Order']['id'] = $this->Session->read('Order.id');
//        $orderData['Order']['complete'] = 1;
//        $orderData['Order']['date'] = date("Y-m-d");
//        $orderData['Order']['status'] = 'Pending';
//
//        $this->Order->save($orderData);
//    }

    /*
* thanks method
*
* @return void
*/
    public function thanks()
    {
        $this->layout = "modern_layout";
        $this->Session->delete('current_action');
        $this->Session->delete('current_controller');

        $order_from_db = $this->Order->findLastPatientOrder($this->current_user['Patient']['id']);

        $orderData['Order']['id'] = $order_from_db['Order']['id'];
        $orderData['Order']['complete'] = 1;
        $orderData['Order']['date'] = date("Y-m-d");
        $orderData['Order']['status'] = 'Pending';
        $this->Order->save($orderData);
        $this->Session->delete('Referral.token');
    }

    /*
    *  overview method
    *
    * @return void
    */
//    public function overview_old($id)
//	{
//		$id = ($id) ?: $this->Session->read('Order.id');
//
//		$this->Order->recursive = 0;
//		$order = $this->Order->findById($id);
//
//		// Is this the order that is processed (saved in session)
//		$isCurrent = $this->Order->isCurrent($order);
//
//		// Validate order owner
//		$validateOwner = ($this->current_user) ?
//			($isCurrent || $this->Order->isOwner($order, $this->current_user['Patient']['id'])) : false;
//
//		if(!$validateOwner || !$order) {
//			$this->redirect(array('action' => 'identify'));
//			return;
//		}
//
//        $user = $this->Order->User->find('first', array(
//            'conditions' => array(
//                'User.id' => $this->current_user['Patient']['user_id']
//            )
//        ));
//        $patient_photo = $this->Order->Patient->PatientPhoto->find('first', array(
//            'conditions' => array(
//                'PatientPhoto.patient_id' => $this->current_user['Patient']['id']
//            ),
//            'order' => 'PatientPhoto.created DESC',
//            'limit' => 1
//        ));
//        $order_meds = $this->Order->OrdersMed->find('all', array(
//            'conditions' => array(
//                'OrdersMed.id' => $order['Order']['id']
//            )
//        ));
//
//        $meds = array();
//        for ($i = 0; $i< count($order_meds); $i++) {
//            array_push($meds, $order_meds[$i]['Med']['name']);
//        }
//
//        $order['User'] = $user['User'];
//        $order['PatientPhoto'] = $patient_photo['PatientPhoto'];
//        $order['Med'] = $meds;
//
//		$this->set(compact('order'));
//    }

    /*
*  overview method
*
* @return void
*/
    public function overview($id)
    {
        $this->layout = "modern_layout";
        $id = ($id) ?: $this->Session->read('Order.id');

        $this->Order->recursive = 0;

        $order = $this->Order->findLastPatientOrder($this->current_user['Patient']['id']);
        //$order = $this->Order->findById($id);

        // Is this the order that is processed (saved in session)
        $isCurrent = $this->Order->isCurrent($order);

        // Validate order owner
        $validateOwner = ($this->current_user) ?
            ($isCurrent || $this->Order->isOwner($order, $this->current_user['Patient']['id'])) : false;

        if (!$validateOwner || !$order) {
            $this->redirect(array('action' => 'identify'));
            return;
        }

        $user = $this->Order->User->find('first', array(
            'conditions' => array(
                'User.id' => $this->current_user['Patient']['user_id']
            )
        ));
        $patient_photo = $this->Order->Patient->PatientPhoto->find('first', array(
            'conditions' => array(
                'PatientPhoto.patient_id' => $this->current_user['Patient']['id']
            ),
            'order' => 'PatientPhoto.created DESC',
            'limit' => 1
        ));
        $order_meds = $this->Order->OrdersMed->find('all', array(
            'conditions' => array(
                'OrdersMed.id' => $order['Order']['id']
            )
        ));

        $meds = array();
        for ($i = 0; $i < count($order_meds); $i++) {
            array_push($meds, $order_meds[$i]['Med']['name']);
        }

        $order['User'] = $user['User'];
        $order['PatientPhoto'] = $patient_photo['PatientPhoto'];
        $order['Med'] = $meds;

        $this->set(compact('order'));
    }


    /*
    * renew method
    *
    * @return void
    */
    public function renew()
    {
        $this->set('nav_step', 4);
        if (!$this->current_user) {
            return $this->redirect(array('action' => 'identify'));
        }
        if ($this->request->is('post')) {
            if ($this->request->data['Order']['renew'] == 'Continue') {
                $user = $this->current_user;
                $order = $this->Order->find('first', array(
                    'conditions' => array(
                        'Order.patient_id' => (integer)$this->current_user['Patient']['id']
                    ),
                    'order' => 'Order.date DESC',
                    'limit' => 1
                ));
                unset($order['Order']['id']);
                unset($order['Order']['paid']);
                unset($order['Order']['paypal_code']);
                unset($order['Order']['complete']);
                unset($order['Order']['terms']);
                unset($order['Order']['consent']);
                $order['Order']['continued'] = 1;
                $order['Order']['short'] = 1;
                $order['Order']['date'] = date("Y-m-d");
                $order['Order']['user_id'] = $user['User']['id'];
                $this->Order->create();
                if ($this->Order->save($order['Order'])) {
                    unset($order['Questionnaire']['id']);
                    $order['Questionnaire']['order_id'] = $this->Order->id;
                    $this->Order->Questionnaire->save($order['Questionnaire']);
                    foreach ($order['OrdersMed'] as $key => $med) {
                        unset($order['OrdersMed'][$key]['id']);
                        $order['OrdersMed'][$key]['order_id'] = $this->Order->id;
                    }
                    $this->Order->OrdersMed->saveAll($order['OrdersMed']);
                    return $this->redirect(array('action' => 'payment'));
                }
            } else if ($this->request->data['Order']['renew'] == 'Change') {
                $order = $this->Order->find('first', array(
                    'conditions' => array(
                        'Order.patient_id' => (integer)$this->current_user['Patient']['id']
                    ),
                    'order' => 'Order.date DESC',
                    'limit' => 1
                ));
                $this->Session->write('Order.continued', 1);
                $this->Session->write('Order.id', null);
                $this->Session->write('Order.ins_company', $order['Order']['ins_company']);
                $this->Session->write('Order.ins_name', $order['Order']['ins_name']);
                $this->Session->write('Order.ins_group', $order['Order']['ins_group']);
                $this->Session->write('Order.ins_num', $order['Order']['ins_num']);
                $this->Session->write('Order.ins_pcn', $order['Order']['ins_pcn']);
                $this->Session->write('Order.ins_bin', $order['Order']['ins_bin']);
                $this->Session->write('Order.ins_phone', $order['Order']['ins_phone']);
                return $this->redirect(array('action' => 'identify'));
            }
        }
        $fine_age = $this->Session->read('Order.fine_age');
        $this->set(compact('fine_age'));
    }


    /*
    * long_renew method
    *
    * @return void
    */
    public function long_renew()
    {
        $this->set('nav_step', 4);
        if (!$this->current_user) {
            return $this->redirect(array('action' => 'identify'));
        }
        if ($this->request->is('post')) {
            if ($this->request->data['Order']['renew'] == 'Continue') {
                $this->Session->write('Order.continued', 1);
                $order = $this->Order->find('first', array(
                    'conditions' => array(
                        'user_id' => $this->current_user['User']['id']
                    ),
                    'order' => 'Order.date DESC',
                    'limit' => 1
                ));
                $this->Session->write('Order.ins_company', $order['Order']['ins_company']);
                $this->Session->write('Order.ins_name', $order['Order']['ins_name']);
                $this->Session->write('Order.ins_group', $order['Order']['ins_group']);
                $this->Session->write('Order.ins_num', $order['Order']['ins_num']);
                $this->Session->write('Order.ins_pcn', $order['Order']['ins_pcn']);
                $this->Session->write('Order.ins_bin', $order['Order']['ins_bin']);
                $this->Session->write('Order.ins_phone', $order['Order']['ins_phone']);
                return $this->redirect(array('action' => 'identify'));
            } else if ($this->request->data['Order']['renew'] == 'Change') {
                $this->Session->write('Order.continued', -1);
                $order = $this->Order->find('first', array(
                    'conditions' => array(
                        'user_id' => $this->current_user['User']['id']
                    ),
                    'order' => 'Order.date DESC',
                    'limit' => 1
                ));
                $this->Session->write('Order.ins_company', $order['Order']['ins_company']);
                $this->Session->write('Order.ins_name', $order['Order']['ins_name']);
                $this->Session->write('Order.ins_group', $order['Order']['ins_group']);
                $this->Session->write('Order.ins_num', $order['Order']['ins_num']);
                $this->Session->write('Order.ins_pcn', $order['Order']['ins_pcn']);
                $this->Session->write('Order.ins_bin', $order['Order']['ins_bin']);
                $this->Session->write('Order.ins_phone', $order['Order']['ins_phone']);
                return $this->redirect(array('action' => 'identify'));
            }
        }
        $fine_age = $this->Session->read('Order.fine_age');
        $this->set(compact('fine_age'));
    }


    /**
     * verify method of PayPal
     *
     * @return void
     */
    public function verify()
    {
        $ppl = WWW_ROOT . 'files' . DS . 'pay_pal_log.txt'; // Pay Pal Log File
        $fh = fopen($ppl, 'a+') or die("can't open file");
        //write to file about start
        $Data = 'START LOG...';
        fwrite($fh, $Data . "\n");

        if ($this->request->is('post')) {
            // read the post from PayPal system and add 'cmd'
            $req = 'cmd=_notify-validate';
            foreach ($this->request->data as $key => $value) {
                $value = urlencode(stripslashes($value));
                $req .= "&$key=$value";
            }
            $fileData = $req;

            //write to file about payment
            $Data = 'testmode = ' . $this->cscsettings['testmode'] . ' ';
            $Data .= 'mc_gross = ' . $this->request->data['mc_gross'] . ' ';
            $Data .= 'mc_currency = ' . $this->request->data['mc_currency'] . ' ';
            $Data .= 'payment_status = ' . $this->request->data['payment_status'] . ' ';
            fwrite($fh, $Data . "\n");
            if (
                (
                    $this->cscsettings['testmode'] &&
                    (
                        $this->request->data['mc_gross'] == "29" ||
                        $this->request->data['mc_gross'] == "29.00"
                    ) &&
                    $this->request->data['mc_currency'] == "USD" &&
                    $this->request->data['payment_status'] == "Completed"
                ) ||
                (
                    $this->cscsettings['testmode'] &&
                    (
                        $this->request->data['mc_gross'] == "10" ||
                        $this->request->data['mc_gross'] == "10.00"
                    ) &&
                    $this->request->data['mc_currency'] == "USD" &&
                    $this->request->data['payment_status'] == "Completed"
                ) ||
                (
                    (
                        $this->request->data['mc_gross'] == "29" ||
                        $this->request->data['mc_gross'] == "29.00"
                    ) &&
                    $this->request->data['mc_currency'] == "USD" &&
                    $this->request->data['payment_status'] == "Completed"
                ) ||
                (
                    (
                        $this->request->data['mc_gross'] == "10" ||
                        $this->request->data['mc_gross'] == "10.00"
                    ) &&
                    $this->request->data['mc_currency'] == "USD" &&
                    $this->request->data['payment_status'] == "Completed"
                ) ||
                (
                    (
                        $this->request->data['mc_gross'] == "00.01"
                    ) &&
                    $this->request->data['mc_currency'] == "USD" &&
                    $this->request->data['payment_status'] == "Completed"
                )
            ) {
                // post back to PayPal system to validate

                if ($this->cscsettings['testmode']) {
                    $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
                    $header .= "Host: www.sandbox.paypal.com\r\n";
                    $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                    $header .= "Content-Length: " . strlen($req) . "\r\n";
                    $header .= "Connection: Close\r\n\r\n";
                    $fp = fsockopen('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
                } else {
                    $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
                    $header .= "Host: www.paypal.com\r\n";
                    $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                    $header .= "Content-Length: " . strlen($req) . "\r\n";
                    $header .= "Connection: Close\r\n\r\n";
                    $fp = fsockopen('ssl://www.paypal.com', 443, $errno, $errstr, 30);
                }

                if (!$fp) {
                    $fileData = date("Y-m-d H:i:s") . " :: " . $fileData;
                    fwrite($fh, $fileData . "\n");


                    // HTTP ERROR
                    $fileData = "HTTP ERROR: " . $fileData;
                } else {
                    //write to file about open socket
                    $Data = '.............SOCKET OPEN..................';
                    fwrite($fh, $Data . "\n");

                    fputs($fp, $header . $req);
                    while (!feof($fp)) {
                        $res = fgets($fp, 1024);
                        if (strcmp($res, "VERIFIED") == 0) {

                            //write to file about open socket
                            $Data = '........................PAYMENT VERIFIED.........................';
                            fwrite($fh, $Data . "\n");


                            $fileData = date("Y-m-d H:i:s") . " :: " . $fileData;
                            fwrite($fh, $fileData . "\n");

                            // PAYMENT VALIDATED & VERIFIED!
                            $fileData = "VERIFIED";

                            $this->Order->recursive = 0;
                            $check = $this->Order->find('first', array(
                                'conditions' => array(
                                    'Order.id' => $this->request->data['custom']
                                ),
                                'order' => 'Order.date DESC',
                                'limit' => 1
                            ));

                            //old order_id --> $this->request->data['option_selection1'];
                            if (!is_null($this->request->data['option_selection1'])) {
                                $orderId = $this->request->data['option_selection1'];
                            } else {
                                $orderId = $this->request->data['option_selection1_1'];
                            }

                            $Data = '...................................ORDER ID: ' . $orderId . '..................................';
                            $Data .= '...................................PAYPAL CODE: ' . $this->request->data['txn_id'] . '..................................';
                            fwrite($fh, $Data . "\n");

                            $paypal_code = $this->request->data['txn_id'];
                            $order_cost = $this->request->data['payment_gross'];

                            // Fix order's duplicate when orderId is empty
                            // $this->Order->set('id', $orderId);
                            // $this->Order->paid($paypal_code, true, $order_cost);
                        } else if (strcmp($res, "INVALID") == 0) {
                            // PAYMENT INVALID & INVESTIGATE MANUALY!
                            $fileData = "INVALID: " . $fileData;

                            $fileData = date("Y-m-d H:i:s") . " :: " . $fileData;
                            fwrite($fh, $fileData . "\n");
                        }
                    }
                    fclose($fp);
                }
            } else {
                // INCORRECT PAYMENT AMOUNT
                $fileData = "WRONG PAYMENT: " . $fileData;
                fwrite($fh, $fileData . "\n");
            }
        }
        fclose($fh);
        die;
    }

    private function get_order($id = null, $recursive = 0)
    {
        if (!isset($id) || !is_numeric($id)) {
            throw new GeneralException(array('msg' => 'Order id was not specified.'));
        }
        if ($id === null) {
            $id = $this->request->data['Order']['id'];
        }
        $order = $this->Order->find('first', array(
            'conditions' => array('Order.id' => $id),
            'contain' => array('AcneClass', 'Treatment', 'Provider', 'Patient'),
            'recursive' => $recursive
        ));
        if (!$order) {
            throw new GeneralException(array('msg' => 'Order not found.'));
        }

        return $order;
    }


    /*
    * admin_index method
    *
    * @return void
    */
    public function admin_index()
    {
        $this->Order->recursive = 0;
        $this->set('orders', $this->Order->find('all', array(
            'conditions' => array('Order.deleted' => 0),
            'order' => 'Order.date DESC',
        )));
    }

    public function admin_history()
    {
        $this->audit_processing();
    }

    private function audit_processing($order_id = null)
    {
        $this->loadModel('Audit');
        $this->loadModel('AuditDelta');

        $this->loadModel('Treatment');

        $treatments = $this->Treatment->find('all');
        $treatments_arr = array();
        for ($i = 0; $i < count($treatments); $i++) {
            $treatments_arr[$treatments[$i]['Treatment']['id']] = 'Class ' . $treatments[$i]['Treatment']['acne_class_id'] . ' / Reg ' . $treatments[$i]['Treatment']['name'];
        }

        if (isset($order_id)) {
            $audits = $this->Audit->find('all', array(
                'conditions' => array(
                    'Audit.entity_id' => $order_id,
                    'Audit.model' => 'Order',
                ),
                'order' => array('Audit.created' => 'desc'),
            ));
        } else {
            $audits = $this->Audit->find('all', array(
                'order' => array('Audit.created' => 'desc')
            ));
        }

        $audit_deltas = $this->AuditDelta->find('all');
        $deltas = array();

        foreach ($audit_deltas as $audit_delta) {
            if (isset($deltas[$audit_delta['AuditDelta']['audit_id']])) {
                if ($audit_delta['AuditDelta']['property_name'] == 'treatment_id') {
                    array_push($deltas[$audit_delta['AuditDelta']['audit_id']], array(
                        'id' => $audit_delta['AuditDelta']['id'],
                        'audit_id' => $audit_delta['AuditDelta']['audit_id'],
                        'property_name' => 'Treatment',
                        'old_value' => $treatments_arr[$audit_delta['AuditDelta']['old_value']],
                        'new_value' => $treatments_arr[$audit_delta['AuditDelta']['new_value']]
                    ));
                } else {
                    array_push($deltas[$audit_delta['AuditDelta']['audit_id']], $audit_delta['AuditDelta']);
                }
            } else {
                if ($audit_delta['AuditDelta']['property_name'] == 'treatment_id') {
                    $deltas[$audit_delta['AuditDelta']['audit_id']] = array(array(
                        'id' => $audit_delta['AuditDelta']['id'],
                        'audit_id' => $audit_delta['AuditDelta']['audit_id'],
                        'property_name' => 'Treatment',
                        'old_value' => $treatments_arr[$audit_delta['AuditDelta']['old_value']],
                        'new_value' => $treatments_arr[$audit_delta['AuditDelta']['new_value']]
                    ));
                } else {
                    $deltas[$audit_delta['AuditDelta']['audit_id']] = array($audit_delta['AuditDelta']);
                }
            }
        }
        $this->set('audits', $audits);
        $this->set('audit_deltas', $deltas);
    }

    /*
    * admin_view method
    *
    * @param string $id
    * @return void
    */
    public function admin_view($id = null)
    {
        // Add Photos through admin

        if ($this->request->is('post') || $this->request->is('put')) {
            $valid = array(
                'image/jpeg' => 'image/jpeg',
                'image/png' => 'image/png',
            );
            $photos = array(
                'face_photo' => 'face_photo',
                'left_photo' => 'left_photo',
                'right_photo' => 'right_photo',
                'chest_photo' => 'chest_photo',
                'back_photo' => 'back_photo'
            );
            foreach ($photos as $photo) {
                if (isset($this->request->data["Order"][$photo]) && (!$this->request->data["Order"][$photo]['tmp_name'] || !isset($valid[$this->request->data["Order"][$photo]['type']]))) {
                    unset($this->request->data["Order"][$photo]);
                }
            }

            $folder_url = WWW_ROOT . 'files' . DS . 'images' . DS . $this->request->data["Order"]['user_id'] . DS . $this->request->data["Order"]['order_id']; // Set path for image

            // create the folder if it does not exist
            if (!is_dir($folder_url)) {
                mkdir($folder_url, 0755, true);
            }

            foreach ($photos as $photo) {
                if (isset($this->request->data["Order"][$photo])) {
                    $file_ext = strtolower(substr($this->request->data["Order"][$photo]['name'], strrpos($this->request->data["Order"][$photo]['name'], '.')));
                    $filename = $photo . $file_ext;
                    move_uploaded_file($this->request->data["Order"][$photo]['tmp_name'], $folder_url . DS . $filename); // Store file
                }
            }

            $this->setFlash(__('New photos have been added.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-success'
            ));
            return $this->redirect(array('action' => 'view', $id));
        }


        $order = $this->get_order($id);
        $this->set('title_for_layout', 'Order Detail');
        $order['OrdersMed'] = $this->Order->OrdersMed->find('all', array('conditions' => array('OrdersMed.order_id' => $id), 'recursive' => -1));

        $patient_id = (integer)$order['Patient']['id'];
        $order['PatientPhotos'] = $this->Order->Patient->PatientPhoto->find('all', array(
            'conditions' => array(
                'PatientPhoto.patient_id' => $patient_id,
                'PatientPhoto.visible' => 1
            )
        ));
        if ($order['Order']['transaction_type'] == 'promo_code') {
            $this->loadModel('PromoCode');
            $promo = $this->PromoCode->find("first", array('conditions' => array('PromoCode.price' => $order['Order']['order_cost'])));
            $this->set('transaction_type', "Promo code used: " . $promo['PromoCode']['code_id']);
        }
        if ($order['Order']['transaction_type'] == 'new') {
            $this->set('transaction_type', 'New, full price');
        }
        if ($order['Order']['transaction_type'] == 'referral') {
            $this->set('transaction_type', 'By referral');
        }
        if ($order['Order']['transaction_type'] == 'credit_for_referral') {
            $this->set('transaction_type', 'Bonus balance used');
        }
        $private_mesages = $this->PrivateMessage->getAllMessagesByOrderId($id);
        $expired_date = (!is_null($order['Order']['date'])) ? $this->Order->getExpiredDate($order)->format('Y-m-d') : '';
        $this->loadModel('User');
        $user = $this->User->find('first',
            array('conditions' => array('User.id' => $order['Order']['user_id'])
            )
        );
        $this->loadModel('Med');
        $this->loadModel('OrderNote');
        $this->set('order', $order);
        $this->set('expired_date', $expired_date);
        $this->set('full_name', $user['User']['first_name'] . " " . $user['User']['middle_name'] . " " . $user['User']['last_name']);
        $this->set('meds', $this->Med->find('list', array('recursive' => 0)));
        $this->set('insurance_state', $this->Order->getInsuranceState($id));
        $this->set('insurance_created', $this->Order->getInsuranceCreated($id));
        $this->set('notes', $this->OrderNote->getNotes($id));
        $this->set('private_mesages', $private_mesages);
        $this->audit_processing($order['Order']['id']);
    }

    /*
    * admin_edit method
    *
    * @param string $id
    * @return void
    */
    public function admin_edit($id = null)
    {
        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__('Invalid order'));
        }
        $this->set('title_for_layout', 'Edit Order');

        if ($this->request->is('post') || $this->request->is('put')) {

            $order = $this->request->data['Order'];

            $order['treatment_id'] = $order['Order']['treatment_id'];
            if ($this->Order->save($this->request->data)) {
                $this->setFlash(__('The order has been saved'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-success'
                ));

                $order = $this->Order->read();

                /* don't send message about order type change
                $data['PrivateMessage']['recipient_id'] = $order['Patient']['user_id'];
                $data['PrivateMessage']['sender_id'] = $order['Provider']['id'];
                $data['PrivateMessage']['subject'] = 'Regimen has been changed';
                $data['PrivateMessage']['urgent'] = true;
                $data['PrivateMessage']['body'] = 'Your information has been reviewed by our provider, and your regimen has been changed. Please review your new regimen in your order overview.';
                $this->PrivateMessage->create();
                $this->PrivateMessage->save($data);
                */

                //check and update pharmacy phone
                if (($order['Patient']['pharmacy_name'] != $this->request->data['Patient']['pharmacy_name']) ||
                    ($order['Patient']['pharmacy_phone'] != $this->request->data['Patient']['pharmacy_phone'])
                ) {
                    $this->loadModel('Patient');
                    $this->loadModel('Patient');
                    $this->Patient->id = $order['Patient']['id'];
                    $this->Patient->set([
                        'pharmacy_name' => $this->request->data['Patient']['pharmacy_name'],
                        'pharmacy_phone' => $this->request->data['Patient']['pharmacy_phone'],
                        'pharmacy_updated_at' => date("Y-m-d H:i:s")
                    ]);
                    $this->Patient->save();
                }
                $this->redirect(array('action' => 'view', $id));
            } else {
                $this->setFlash(__('The order could not be saved. Please, try again.'), 'alert', array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger'
                ));
            }
        } else {
            $this->request->data = $this->Order->read(null, $id);
        }
        $order = $this->Order->read();
        $acneClasses = $this->Order->AcneClass->find('list');
        $treatments_select = $this->Order->Treatment->getAllTreatmentForSelect();
        $treatments = $this->Order->Treatment->find('list');
        $meds = $this->Order->OrdersMed->Med->find('list');
        $this->set(compact('users', 'acneClasses', 'treatments', 'meds', 'order', 'treatments_select'));
    }

    /*
    * admin_delete method
    *
    * @param string $id
    * @return void
    */
    public function admin_delete($id = null)
    {
        $order = $this->get_order($id);

        if ($this->request->is('post')) {
            $this->Order->id = $id;
            $this->Order->saveField('deleted', 1);
            $this->setFlash(__('Order deleted successfully.'), 'alert', array(
                'plugin' => 'BoostCake',
                'class' => 'alert-success'
            ));
            $this->redirect(array('action' => 'index', 'admin' => true));
        }
        $this->set('order', $order);
    }

    public function admin_export($id = null)
    {
        if (!isset($id) || $id == null) {
            $orders = $this->Order->find('all', array(
                'order' => array('Order.date' => 'desc'),
                'conditions' => array('Order.deleted' => 0),
                'recursive' => 0
            ));
            $this->set('order', $orders);
            $this->set('title', 'csc_orders_' . date('m-d-Y_h:i:sa', time()));
        } else {
            $order = $this->get_order($id, 0);
            $this->set('order', array($order));
            $this->set('title', 'csc_order_' . $id . '_' . date('m-d-Y_h:i:sa', time()));
        }
        $this->set('meds', $this->Order->OrdersMed->Med->find('list'));
        $this->layout = null;
    }

    /**
     * get the acne settings from the Session Order.
     * @throws NotFoundException
     * @return unknown
     */
    private function _getOrderAcne()
    {
        $order = $this->Session->read('Order');

        if (!isset($order)) {
            throw new NotFoundException();
        }

        $acne['acne_forehead'] = $order['acne_forehead'];
        $acne['acne_cheeks'] = $order['acne_cheeks'];
        $acne['acne_chin'] = $order['acne_chin'];
        $acne['acne_chest'] = $order['acne_chest'];
        $acne['acne_back'] = $order['acne_back'];
        $acne['acne_neck'] = $order['acne_neck'];
        $acne['acne_nose'] = $order['acne_nose'];
        $acne['acne_temple'] = $order['acne_temple'];
        return $acne;
    }

    /**
     * Initialize the acne array.
     * @return multitype:string
     */
    private function _initializeAcne()
    {
        $acne = array(
            'acne_forehead' => '00000',
            'acne_cheeks' => '00000',
            'acne_chin' => '00000',
            'acne_chest' => '00000',
            'acne_back' => '00000',
            'acne_neck' => '00000',
            'acne_nose' => '00000',
            'acne_temple' => '00000'
        );
        return $acne;
    }

    public function admin_assign_treatment($id = null)
    {
        if (!$this->Order->exists($id)) {
            throw new NotFoundException(__('Invalid order'));
        }

        $this->Order->set('id', $id);
        if ($this->Order->complete()) {
            $this->Session->setFlash(__('Treatment successfully assigned.'));
            $this->redirect(array('action' => 'view', $id));
        } else {
            $this->Session->setFlash(__('Treatment is not assigned.'));
        }
        $this->redirect(array('action' => 'view', $id));
    }

    public function admin_reassign_treatment($id = null)
    {
        if (!$this->Order->exists($id)) {
            throw new NotFoundException(__('Invalid order'));
        }
        $order = $this->Order->find('first', ['conditions' => ['Order.id' => $id]]);
        $order_status = Order::STATUS_COMPLETED;
        if (($order['Order']['status'] == 'Completed') && ($order['Order']['prescription_sent'] == 1)) {
            $order_status = Order::STATUS_REASSIGN_AFTER_SEND_PRESCRIPTION;
        } elseif ($order['Order']['status'] == 'Completed') {
            $order_status = Order::STATUS_REASSIGN;
        }
        $this->Order->set('id', $id);
        if ($this->Order->complete($order_status)) {
            $this->Session->setFlash(__('Treatment successfully reassigned.'));

            $this->redirect(array('action' => 'view', $id));
        } else {
            $this->Session->setFlash(__('Treatment is not assigned.'));
        }
        $this->redirect(array('action' => 'view', $id));
    }

    public function admin_send_via_fax($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__('Invalid order'));
        }

        $this->Order->set('prescription_faxed', true);
        $this->Order->save();

        $this->Session->setFlash(__('Prescription was sent'));
        $this->redirect(array('action' => 'view', $id));
    }

    public function admin_remove_send_via_fax($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__('Invalid order'));
        }

        $this->Order->set('prescription_faxed', false);
        $this->Order->save();

        $this->Session->setFlash(__('Sending prescription was canceled'));
        $this->redirect(array('action' => 'view', $id));
    }

    public function admin_send_prescription($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__('Invalid order'));
        }

        $order = $this->get_order($id);

        $privateMessage = \ClassRegistry::getObject('PrivateMessage');
        $privateMessage->clear();

        $privateMessage->set('subject', 'Your prescription was sent');
        $privateMessage->setBodyByView('prescription_sent', array('order' => $order));

        $result = $privateMessage->save(array(
            'recipient_id' => $order['Patient']['user_id'],
            'sender_id' => $order['Provider']['id'],
            'urgent' => (int)true,
        ));

        if (!$result) {
            $this->Session->setFlash(__('Something went wrong'));
            $this->redirect(array('action' => 'view', $id));
        }

        $this->Order->set('prescription_sent', true);
        $this->Order->save();

        $this->Session->setFlash(__('Prescription was sent'));
        $this->redirect(array('action' => 'view', $id));
    }


    public function admin_remove_send_prescription($id = null)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }

        $this->Order->id = $id;
        if (!$this->Order->exists()) {
            throw new NotFoundException(__('Invalid order'));
        }

        $this->Order->set('prescription_sent', false);
        $this->Order->save();

        $this->Session->setFlash(__('Sending prescription was canceled'));
        $this->redirect(array('action' => 'view', $id));
    }

    public function getPaypalPromoCode($promo_code = null)
    {
        $promo_code = strtoupper($promo_code);

        $default_price = 29;
        if (!$this->RequestHandler->isAjax()) {
            $response = array(
                'status' => 'error',
                'promo_code' => $promo_code,
                'order_price' => $default_price
            );
            echo json_encode($response);
            return;
        } else {
            if (is_null($promo_code)) {
                $response = array(
                    'status' => 'error',
                    'promo_code' => $promo_code,
                    'order_price' => $default_price
                );
                echo json_encode($response);
                return;
            } else {
                $this->loadModel('PromoCode');
                //$order_type = $this->Order->FalOrAcne($order_id);
                //get correct promo code from DB('promo_codes')

                $promo_code = $this->PromoCode->getPriceWithPromoCodePaypal($promo_code);
                if ($promo_code['promo']) {
                    $accept_text = "Promo code accepted. New price is $" . $promo_code['price'];
                } else {
                    $accept_text = "Promo code isn't accepted";
                }
                $response = array(
                    'status' => 'ok',
                    'promo_code' => $promo_code['code'],
                    'order_type' => $promo_code['type'],
                    'order_price' => $promo_code['price'],
                    'order_accept_text' => $accept_text,
                );
                echo json_encode($response);
                return;
            }
        }
    }

    public function setInsuranceState($order_id, $flag)
    {
        if ($this->Order->updateAll(
            array('Order.insurance_state' => $flag),
            array('Order.id' => $order_id)
        )) {
            //set created time for current insurance state
            $this->Order->setInsuranceCreated($order_id, $flag);

            $order = $this->Order->find('first', array(
                'conditions' => array('Order.id' => $order_id)
            ));
            switch ($flag) {
                case 1:
                    $this->Order->getEventManager()->dispatch(
                        new CakeEvent(
                            'Controller.Order.insuranceRequested',
                            $this,
                            array_merge(
                                array('order' => $order)
                            )));
                    break;
                case 3:
                    $this->Order->getEventManager()->dispatch(
                        new CakeEvent(
                            'Controller.Order.insuranceApproved',
                            $this,
                            array_merge(
                                array('order' => $order)
                            )));
                    break;
                case 4:
                    $this->Order->getEventManager()->dispatch(
                        new CakeEvent(
                            'Controller.Order.insuranceDenied',
                            $this,
                            array_merge(
                                array('order' => $order)
                            )));
                    break;
                default:
                    break;
            }
            $response = array(
                'status' => 'ok',
                'flag' => $flag
            );
            echo json_encode($response);
            return;
        } else {
            $response = array(
                'status' => 'error',
                'flag' => 0
            );
            echo json_encode($response);
            return;
        }
    }

    public function free_paypal($order_id)
    {
        $user_id = $this->Auth->User('id');
        $id = $order_id;
        if ($this->Order->setFreePaypal($order_id, $user_id)) {
            $this->Session->write('paid', 1);
            $this->redirect(array('controller' => 'orders', 'action' => 'patient'));
        } else {
            $this->redirect(array('controller' => 'orders', 'action' => 'payment', $id));
        }
    }

    public function free_paypal_modern($order_id)
    {
        $user_id = $this->Auth->User('id');
        $id = $order_id;
        if ($this->Order->setFreePaypal($order_id, $user_id)) {
            $this->Session->write('paid', 1);
            $this->redirect(array('controller' => 'orders', 'action' => 'patient'));
        } else {
            $this->redirect(array('controller' => 'orders', 'action' => 'payment', $id));
        }
    }

    public function addAdminNote($order_id)
    {
        $this->loadModel('OrderNote');
        $note = $this->request->data['note'];
        if ($this->OrderNote->save([
            'order_id' => $order_id,
            'message' => $note,
        ])) {
            $response = [
                'status' => 'ok',
                'message' => 'Note was saved',
                'note' => $note,
                'created' => date('Y-m-d H:i:s')
            ];
            echo json_encode($response);
            return;
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Message can not blank.'
            ];
            echo json_encode($response);
            return;
        }
    }

    public function resetOrder($order_id, $type)
    {
        $this->Order->id = $order_id;
        $this->Order->set('status', 'Cancelled');
        if ($this->Order->save()) {
            $this->Session->delete('User');
            $this->Session->delete('Patient');
            $this->Session->delete('Order');
            $this->Session->delete('age');
            $this->Session->delete('overview-done');
            $this->Session->delete('paid');
            $this->Session->delete('complete');
            $this->Session->delete('payment_error');
            $this->Session->delete('current_action');
            $this->Session->delete('current_controller');
            return $this->redirect(array('controller' => 'orders', 'action' => 'profile', '?' => ['type' => $type]));
        }
    }

    public function admin_refund($order_id, $refund_type)
    {
        $this->autoRender = false;
        if ($this->Order->setRefundCreated($order_id, $refund_type)) {
            if ($refund_type == 'refund_requested') {
                $this->Order->getEventManager()->dispatch(
                    new CakeEvent(
                        'Controller.Order.refundRequested',
                        $this,
                        array_merge(
                            array('order' => $this->Order->find('first', ['conditions' => ['Order.id' => $order_id]]))
                        )));
            }
            $response = [
                'status' => 'ok',
                'message' => 'Success',
                'created' => date('Y-m-d H:i:s'),
                'type' => $refund_type
            ];
            echo json_encode($response);
            return;
        } else {
            $response = [
                'status' => 'error',
                'message' => 'This order can\'t update.'
            ];
            echo json_encode($response);
            return;
        }
    }

    public function goodrx_widget()
    {
        App::uses('HttpSocket', 'Network/Http');
        $HttpSocket = new HttpSocket();
        $url = "https://api.goodrx.com/fair-price?name=" . $_GET['name'] . "&api_key=07b6456666&sig=" . $_GET['sig'];
        $results = $HttpSocket->get($url);
        echo $results->body;
        return;
    }
}
